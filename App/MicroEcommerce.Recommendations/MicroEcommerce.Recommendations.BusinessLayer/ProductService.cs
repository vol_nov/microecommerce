﻿using System;
using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.Recommendations.Persistence;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Recommendations.BusinessLayer
{
    public interface IProductService : IService<Product, Guid>
    {
        
    }
    
    public class ProductService : Service<Product, Guid>, IProductService
    {
        public ProductService(IProductRepository repository) : base(repository)
        {
        }
    }
}
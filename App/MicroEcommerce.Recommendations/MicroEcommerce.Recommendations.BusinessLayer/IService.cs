﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MicroEcommerce.Recommendations.BusinessLayer
{
    public interface IService<T, TIdentity> where T : class, new()
    {
        Task<ICollection<T>> GetModels();
        Task<T> GetModelById(TIdentity id);
        Task<T> Add(T entity);
        Task Update(T entity);
        Task Delete(TIdentity id);
        Task<ICollection<T>> Where(Expression<Func<T, bool>> condition);
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.Recommendations.Persistence;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Recommendations.BusinessLayer
{
    public interface IRecommendationService : IService<Recommendation, int>
    {
        Task<Recommendation> GetRecommendationsTopRated(int userId, int count);
        Task<Recommendation> GetRecommendationsTopBuyed(int userId, int count);
        Task<Recommendation> GetRecommendationsSimilarCategory(int userId, Guid categoryId, int count);
        Task<Recommendation> GetRecommendationsConnectedWithCategory(int userId, Guid categoryId, int count);
        Task<Recommendation> GetRecommendationsByDiscount(int userId, int count);
    }
    
    public class RecommendationService : Service<Recommendation, int>, IRecommendationService
    {
        private readonly IProductRepository _productRepository;
        private readonly IConnectedCategoryRepository _connectedCategoryRepository;
        
        public RecommendationService(IRecommendationRepository recommendationRepository, 
            IProductRepository productRepository,
            IConnectedCategoryRepository connectedCategoryRepository) : base(recommendationRepository)
        {
            _productRepository = productRepository;
            _connectedCategoryRepository = connectedCategoryRepository;
        }

        public async Task<Recommendation> GetRecommendationsTopRated(int userId, int count)
        {
            var recommendation = new Recommendation
            {
                Type = RecommendationType.TopRated,
                CreationDateTime = DateTime.Now,
                UserId = userId,
                Products = await _productRepository.OrderByDescendingQueryable(p => p.LikesCount)
                    .Select(rp => new RecommendationProduct() { ProductId = rp.ProductId } )
                    .Take(count)
                    .ToListAsync()
            };
            
            return await Repository.Add(recommendation);
        }

        public async Task<Recommendation> GetRecommendationsTopBuyed(int userId, int count)
        {
            var recommendation = new Recommendation
            {
                Type = RecommendationType.TopBuyed,
                CreationDateTime = DateTime.Now,
                UserId = userId,
                Products = await _productRepository.OrderByDescendingQueryable(p => p.BoughtCount)
                    .Select(rp => new RecommendationProduct() { ProductId = rp.ProductId } )
                    .Take(count)
                    .ToListAsync()
            };

            return await Repository.Add(recommendation);
        }

        public async Task<Recommendation> GetRecommendationsSimilarCategory(int userId, Guid categoryId, int count)
        {
            var recommendation = new Recommendation
            {
                Type = RecommendationType.SimilarCategory,
                CreationDateTime = DateTime.Now,
                UserId = userId,
                Products = await _productRepository.WhereQueryable(p =>
                        p.ProductCategories.Select(pc => pc.CategoryId).Contains(categoryId))
                    .Select(rp => new RecommendationProduct() { ProductId = rp.ProductId } )
                    .Take(count)
                    .ToListAsync()
            };

            return await Repository.Add(recommendation);
        }

        public async Task<Recommendation> GetRecommendationsConnectedWithCategory(int userId, Guid categoryId, int count)
        {
            var connectedCategories = await _connectedCategoryRepository.GetConnectedCategories(categoryId);
            var connectedCategoriesGuids = connectedCategories.Select(cc => cc.CategoryId);
            
            var products = await _productRepository.WhereQueryable(p => p.ProductCategories
                .Select(pc => pc.CategoryId).Intersect(connectedCategoriesGuids).Any())
                .Take(count).ToListAsync();
            
            var recommendation = new Recommendation
            {
                Type = RecommendationType.ConnectedWithCategory,
                CreationDateTime = DateTime.Now,
                UserId = userId,
                Products = products.Select(rp => new RecommendationProduct() { ProductId = rp.ProductId } ).ToList()
            };

            return await Repository.Add(recommendation);
        }

        public async Task<Recommendation> GetRecommendationsByDiscount(int userId, int count)
        {
            var recommendation = new Recommendation
            {
                Type = RecommendationType.ByDiscount,
                CreationDateTime = DateTime.Now,
                UserId = userId,
                Products = (await _productRepository.Where(p => p.Discounts.Any()))
                    .Select(rp => new RecommendationProduct() { ProductId = rp.ProductId } ).ToList()
            };

            return await Repository.Add(recommendation);
        }
    }
}
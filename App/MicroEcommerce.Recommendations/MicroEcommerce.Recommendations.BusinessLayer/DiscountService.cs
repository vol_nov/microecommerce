﻿using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.Recommendations.Persistence;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Recommendations.BusinessLayer
{
    public interface IDiscountService : IService<Discount, int>
    {
        
    }
    
    public class DiscountService : Service<Discount, int>, IDiscountService
    {
        public DiscountService(IDiscountRepository repository) : base(repository)
        {
        }
    }
}
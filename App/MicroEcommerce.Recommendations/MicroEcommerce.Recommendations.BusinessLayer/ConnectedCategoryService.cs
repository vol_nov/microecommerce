﻿using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.Recommendations.Persistence;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Recommendations.BusinessLayer
{
    public interface IConnectedCategoryService : IService<ConnectedCategory, int>
    {
        
    }
    
    public class ConnectedCategoryService : Service<ConnectedCategory, int>, IConnectedCategoryService
    {
        public ConnectedCategoryService(IConnectedCategoryRepository repository) : base(repository)
        {
        }
    }
}
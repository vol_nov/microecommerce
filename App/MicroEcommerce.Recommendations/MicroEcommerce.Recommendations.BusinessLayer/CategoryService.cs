﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.Recommendations.Persistence;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Recommendations.BusinessLayer
{
    public interface ICategoryService : IService<Category, Guid>
    {
        
    }
    
    public class CategoryService : Service<Category, Guid>, ICategoryService
    {
        public CategoryService(ICategoryRepository repository) : base(repository)
        {
        }
    }
}
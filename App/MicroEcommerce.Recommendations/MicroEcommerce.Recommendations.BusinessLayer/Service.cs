﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Recommendations.BusinessLayer
{
    public abstract class Service<T, TIdentity> : IService<T, TIdentity> where T : class, new()
    {
        protected readonly IRepository<T, TIdentity> Repository;

        protected Service(IRepository<T, TIdentity> repository)
        {
            Repository = repository;
        }

        public async Task<ICollection<T>> GetModels()
        {
            return await Repository.GetEntities();
        }

        public async Task<T> GetModelById(TIdentity id)
        {
            return await Repository.GetEntityById(id);
        }

        public async Task<T> Add(T entity)
        {
            return await Repository.Add(entity);
        }

        public async Task Update(T entity)
        {
            await Repository.Update(entity);
        }

        public async Task Delete(TIdentity id)
        {
            await Repository.Delete(id);
        }

        public async Task<ICollection<T>> Where(Expression<Func<T, bool>> condition)
        {
            return await Repository.Where(condition);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Recommendations.Persistence
{
    public interface IRecommendationRepository : IRepository<Recommendation, int>
    {
//        Task<ICollection<Recommendation>> GetRecommendationsTopRated();
//        Task<ICollection<Recommendation>> GetRecommendationsTopBuyed();
//        Task<ICollection<Recommendation>> GetRecommendationsSimilarCategory(Guid categoryId);
//        Task<ICollection<Recommendation>> GetRecommendationsConnectedWithCategory(Guid categoryId);
//        Task<ICollection<Recommendation>> GetRecommendationsByDiscount();
    }
    
    public class RecommendationRepository : Repository<Recommendation, int, RecommendationsContext>, IRecommendationRepository
    {
        public RecommendationRepository(RecommendationsContext context) : base(context)
        {
        }

        protected override DbSet<Recommendation> Table => Context.Recommendations;
        protected override IQueryable<Recommendation> Include(DbSet<Recommendation> table)
        {
            return table.Include(r => r.Products).AsNoTracking();
        }

        public override async Task<Recommendation> GetEntityById(int id)
        {
            return await Table.FirstOrDefaultAsync(t => t.RecommendationId == id);
        }

//        public async Task<ICollection<Recommendation>> GetRecommendationsTopRated()
//        {
//            return await Table.OrderByDescending(r => r.Products.Max(p => p.LikesCount)).ToListAsync();
//        }
//
//        public Task<ICollection<Recommendation>> GetRecommendationsTopBuyed()
//        {
//            throw new NotImplementedException();
//        }
//
//        public Task<ICollection<Recommendation>> GetRecommendationsSimilarCategory(Guid categoryId)
//        {
//            throw new NotImplementedException();
//        }
//
//        public Task<ICollection<Recommendation>> GetRecommendationsConnectedWithCategory(Guid categoryId)
//        {
//            throw new NotImplementedException();
//        }
//
//        public Task<ICollection<Recommendation>> GetRecommendationsByDiscount()
//        {
//            throw new NotImplementedException();
//        }
    }
}
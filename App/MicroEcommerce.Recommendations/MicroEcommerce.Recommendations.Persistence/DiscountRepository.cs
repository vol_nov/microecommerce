﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Recommendations.Persistence
{
    public interface IDiscountRepository : IRepository<Discount, int>
    {
        
    }
    
    public class DiscountRepository : Repository<Discount, int, RecommendationsContext>, IDiscountRepository
    {
        public DiscountRepository(RecommendationsContext context) : base(context)
        {
        }

        protected override DbSet<Discount> Table => Context.Discounts;
        protected override IQueryable<Discount> Include(DbSet<Discount> table)
        {
            return Table.AsNoTracking();
        }

        public override Task<Discount> GetEntityById(int id)
        {
            return Table.FirstOrDefaultAsync(d => d.DiscountId == id);
        }
    }
}
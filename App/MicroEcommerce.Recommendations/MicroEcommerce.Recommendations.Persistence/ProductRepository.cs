﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Recommendations.Persistence
{
    public interface IProductRepository : IRepository<Product, Guid>
    {
        
    }
    
    public class ProductRepository : Repository<Product, Guid, RecommendationsContext>, IProductRepository
    {
        public ProductRepository(RecommendationsContext context) : base(context)
        {
        }

        protected override DbSet<Product> Table => Context.Products;
        protected override IQueryable<Product> Include(DbSet<Product> table)
        {
            return table.Include(p => p.ProductCategories)
                        .Include(p => p.Discounts).AsNoTracking();
        }

        public override async Task<Product> GetEntityById(Guid id)
        {
            return await Table.FirstOrDefaultAsync(p => p.ProductId == id);
        }
    }
}
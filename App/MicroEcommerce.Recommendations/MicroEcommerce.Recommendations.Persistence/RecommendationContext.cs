﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using MicroEcommerce.Recommendations.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace MicroEcommerce.Recommendations.Persistence
{
    public class RecommendationsContext : DbContext, IDesignTimeDbContextFactory<RecommendationsContext>
    {
        private readonly string _connectionString;
        
        public DbSet<Category> Categories { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Recommendation> Recommendations { get; set; }
        public DbSet<RecommendationProduct> RecommendationProducts { get; set; }
        public DbSet<ConnectedCategory> ConnectedCategories { get; set; }
        
        public RecommendationsContext()
        {
            
        }
        
        public RecommendationsContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        public RecommendationsContext CreateDbContext(string[] args)
        {
            return new RecommendationsContext("Server=NOTEBOOK;Database=MicroEcommerce.Recommendations;Trusted_Connection=True;");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasKey(c => c.CategoryId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.None);
            modelBuilder.Entity<Discount>().HasKey(d => d.DiscountId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.None);
            modelBuilder.Entity<Product>().HasKey(p => p.ProductId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.None);
            modelBuilder.Entity<ProductCategory>().HasKey(pc => pc.ProductCategoryId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Recommendation>().HasKey(r => r.RecommendationId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RecommendationProduct>().HasKey(rp => rp.RecommendationProductId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<ConnectedCategory>().HasKey(sc => sc.ConnectedCategoryId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
        }
    }
}
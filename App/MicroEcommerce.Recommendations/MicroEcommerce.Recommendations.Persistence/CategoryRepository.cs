﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Recommendations.Persistence
{
    public interface ICategoryRepository : IRepository<Category, Guid>
    {
        
    }
    
    public class CategoryRepository : Repository<Category, Guid, RecommendationsContext>, ICategoryRepository
    {
        public CategoryRepository(RecommendationsContext context) : base(context)
        {
        }

        protected override DbSet<Category> Table => Context.Categories;
        protected override IQueryable<Category> Include(DbSet<Category> table)
        {
            return table.AsNoTracking();
        }

        public override async Task<Category> GetEntityById(Guid id)
        {
            return await Table.FirstOrDefaultAsync(c => c.CategoryId == id);
        }
    }
}
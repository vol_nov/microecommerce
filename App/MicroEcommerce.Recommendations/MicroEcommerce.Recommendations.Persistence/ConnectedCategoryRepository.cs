﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Recommendations.Persistence
{
    public interface IConnectedCategoryRepository : IRepository<ConnectedCategory, int>
    {
        Task<ICollection<Category>> GetConnectedCategories(Guid categoryId);
    }
    
    public class ConnectedCategoryRepository : Repository<ConnectedCategory, int, RecommendationsContext>, IConnectedCategoryRepository 
    {
        public ConnectedCategoryRepository(RecommendationsContext context) : base(context)
        {
        }

        protected override DbSet<ConnectedCategory> Table => Context.ConnectedCategories;
        protected override IQueryable<ConnectedCategory> Include(DbSet<ConnectedCategory> table)
        {
            return table.AsNoTracking();
        }

        public override async Task<ConnectedCategory> GetEntityById(int id)
        {
            return await Table.FirstOrDefaultAsync(sc => sc.ConnectedCategoryId == id);
        }
        
        public async Task<ICollection<Category>> GetConnectedCategories(Guid categoryId)
        {
            var similarCategoriesIds = Context.ConnectedCategories.Where(s =>
                    s.MasterCategory == categoryId || s.SlaveCategory == categoryId)
                .Select(s => s.MasterCategory == categoryId ? s.SlaveCategory : s.MasterCategory);

            var similarCategories = await Context.Categories.Where(c => similarCategoriesIds.Contains(c.CategoryId))
                .ToListAsync();
            return similarCategories;
        }
    }
}
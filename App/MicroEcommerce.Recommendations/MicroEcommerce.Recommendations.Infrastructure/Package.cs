﻿using System;
using MicroEcommerce.PubSub;
using MicroEcommerce.Recommendations.BusinessLayer;
using MicroEcommerce.Recommendations.Persistence;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace MicroEcommerce.Recommendations.Infrastructure
{
    public class Package : IPackage
    {
        public void RegisterServices(Container container)
        {
            RegisterBusinessLayerDependencies(container);
            RegisterPersistenceDependencies(container);
        }

        public void RegisterBusinessLayerDependencies(Container container)
        {
            container.Register<ICategoryService, CategoryService>(Lifestyle.Scoped);
            container.Register<IDiscountService, DiscountService>(Lifestyle.Scoped);
            container.Register<IProductService, ProductService>(Lifestyle.Scoped);
            container.Register<IRecommendationService, RecommendationService>(Lifestyle.Scoped);
            container.Register<IConnectedCategoryService, ConnectedCategoryService>(Lifestyle.Scoped);
        }

        public void RegisterPersistenceDependencies(Container container)
        {
            container.Register(() =>
            {
                var connectionString =
                    container.GetInstance<IConfigurationRoot>().GetConnectionString("DefaultConnectionString");
            
                return new RecommendationsContext(connectionString);
            }, Lifestyle.Scoped);   
            
            container.Register<ICategoryRepository, CategoryRepository>(Lifestyle.Scoped);
            container.Register<IDiscountRepository, DiscountRepository>(Lifestyle.Scoped);
            container.Register<IProductRepository, ProductRepository>(Lifestyle.Scoped);
            container.Register<IRecommendationRepository, RecommendationRepository>(Lifestyle.Scoped);
            container.Register<IConnectedCategoryRepository, ConnectedCategoryRepository>(Lifestyle.Scoped);

            container.Register<ISendExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new SendExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);

            container.Register<IReceiveExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new ReceiveExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);
        }
    }
}
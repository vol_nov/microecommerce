﻿using System;
using System.Collections.Generic;

namespace MicroEcommerce.Recommendations.Models
{
    [Serializable]
    public class Category
    {
        public Guid CategoryId { get; set; }
        public ICollection<ConnectedCategory> SimilarCategories { get; set; } 
    }
}
﻿using System;
using System.Collections.Generic;

namespace MicroEcommerce.Recommendations.Models
{
    [Serializable]
    public class Recommendation
    {
        public int RecommendationId { get; set; }
        public int UserId { get; set; }
        public RecommendationType Type { get; set; }
        public List<RecommendationProduct> Products { get; set; }
        public DateTime CreationDateTime { get; set; }
    }
}
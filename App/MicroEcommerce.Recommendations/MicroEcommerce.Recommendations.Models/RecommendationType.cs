using System;

namespace MicroEcommerce.Recommendations.Models
{
    [Serializable]
    public enum RecommendationType
    {
        TopRated, TopBuyed, SimilarCategory, ConnectedWithCategory, ByDiscount
    }
}

    
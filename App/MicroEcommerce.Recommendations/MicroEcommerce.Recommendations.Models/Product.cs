﻿using System;
using System.Collections.Generic;

namespace MicroEcommerce.Recommendations.Models
{
    [Serializable]
    public class Product
    {
        public Guid ProductId { get; set; }
        public decimal Price { get; set; }
        public int LikesCount { get; set; }
        public int BoughtCount { get; set; }
        public ICollection<ProductCategory> ProductCategories { get; set; }
        public ICollection<Discount> Discounts { get; set; }
    }
}
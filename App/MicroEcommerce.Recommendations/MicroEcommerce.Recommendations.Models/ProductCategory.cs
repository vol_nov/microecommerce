﻿using System;

namespace MicroEcommerce.Recommendations.Models
{
    [Serializable]
    public class ProductCategory
    {
        public int ProductCategoryId { get; set; }
        public Guid ProductId { get; set; }
        public Guid CategoryId { get; set; }
    }
}
﻿using System;

namespace MicroEcommerce.Recommendations.Models
{
    [Serializable]
    public class ConnectedCategory
    {
        public int ConnectedCategoryId { get; set; }
        public Guid MasterCategory { get; set; }
        public Guid SlaveCategory { get; set; }
    }
}
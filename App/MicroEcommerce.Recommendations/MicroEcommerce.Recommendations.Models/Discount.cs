﻿using System;

namespace MicroEcommerce.Recommendations.Models
{
    [Serializable]
    public class Discount
    {
        public int DiscountId { get; set; }
        public decimal Value { get; set; }
        public Guid ProductId { get; set; }
    }
}
﻿using System;

namespace MicroEcommerce.Recommendations.Models
{
    [Serializable]
    public class RecommendationProduct
    {
        public int RecommendationProductId { get; set; }
        public int RecommendationId { get; set; }
        public Guid ProductId { get; set; }
    }
}
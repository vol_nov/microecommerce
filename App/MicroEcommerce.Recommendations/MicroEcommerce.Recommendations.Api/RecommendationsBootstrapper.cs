﻿using MicroEcommerce.Recommendations.Infrastructure;
using MicroEcommerce.SimpleInjector.Nancy;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace MicroEcommerce.Recommendations.Api
{
    public class RecommendationsBootstrapper : SimpleInjectorNancyBootstrapper
    {
        public RecommendationsBootstrapper(IConfigurationRoot configurationRoot, ILogger logger) : base(configurationRoot, logger)
        {
            Packages = new[] {new Package()};
        }
    }
}
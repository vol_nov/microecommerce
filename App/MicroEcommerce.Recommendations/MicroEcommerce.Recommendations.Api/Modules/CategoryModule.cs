﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.BusinessLayer;
using MicroEcommerce.Recommendations.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Recommendations.Api.Modules
{
    public class CategoryModule : NancyModule
    {
        private readonly ICategoryService _categoryService;

        public CategoryModule(ICategoryService categoryService) : base("/api/recommendations")
        {
            _categoryService = categoryService;

            Get("/category", GetCategories);
            Get("/category/{id}", GetCategoryById);
            Post("/category", AddCategory);
            Put("/category", UpdateCategory);
            Delete("/category/{id}", DeleteCategory);
        }

        public async Task<object> GetCategories(dynamic parameters)
        {
            var categories = await _categoryService.GetModels();
            return Negotiate.WithModel(categories).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetCategoryById(dynamic parameters)
        {
            var category = await _categoryService.GetModelById((Guid) parameters.id);
            return Negotiate.WithModel(category).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddCategory(dynamic parameters)
        {
            var category = this.Bind<Category>();
            await _categoryService.Add(category);
            return Negotiate.WithModel(category).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateCategory(dynamic parameters)
        {
            var category = this.Bind<Category>();
            await _categoryService.Update(category);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteCategory(dynamic parameters)
        {
            Guid categoryId = parameters.id;
            await _categoryService.Delete(categoryId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.BusinessLayer;
using MicroEcommerce.Recommendations.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Recommendations.Api.Modules
{
    public class ProductModule : NancyModule
    {
        private readonly IProductService _productService;

        public ProductModule(IProductService productService) : base("/api/recommendations")
        {
            _productService = productService;

            Get("/product", GetProducts);
            Get("/product/{id}", GetProductById);
            Post("/product", AddProduct);
            Put("/product", UpdateProduct);
            Delete("/product/{id}", DeleteProduct);
        }

        public async Task<object> GetProducts(dynamic parameters)
        {
            var products = await _productService.GetModels();
            return Negotiate.WithModel(products).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetProductById(dynamic parameters)
        {
            var product = await _productService.GetModelById((Guid) parameters.id);
            return Negotiate.WithModel(product).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddProduct(dynamic parameters)
        {
            var product = this.Bind<Product>();
            await _productService.Add(product);
            return Negotiate.WithModel(product).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateProduct(dynamic parameters)
        {
            var product = this.Bind<Product>();
            await _productService.Update(product);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteProduct(dynamic parameters)
        {
            Guid productId = parameters.id;
            await _productService.Delete(productId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
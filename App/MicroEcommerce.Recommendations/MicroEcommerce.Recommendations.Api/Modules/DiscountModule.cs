﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.BusinessLayer;
using MicroEcommerce.Recommendations.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Recommendations.Api.Modules
{
    public class DiscountModule : NancyModule
    {
        private readonly IDiscountService _discountService;

        public DiscountModule(IDiscountService discountService) : base("/api/recommendations")
        {
            _discountService = discountService;

            Get("/discount", GetDiscounts);
            Get("/discount/{id}", GetDiscountById);
            Post("/discount", AddDiscount);
            Put("/discount", UpdateDiscount);
            Delete("/discount/{id}", DeleteDiscount);
        }

        public async Task<object> GetDiscounts(dynamic parameters)
        {
            var discounts = await _discountService.GetModels();
            return Negotiate.WithModel(discounts).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetDiscountById(dynamic parameters)
        {
            var discount = await _discountService.GetModelById((int) parameters.id);
            return Negotiate.WithModel(discount).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddDiscount(dynamic parameters)
        {
            var discount = this.Bind<Discount>();
            await _discountService.Add(discount);
            return Negotiate.WithModel(discount).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateDiscount(dynamic parameters)
        {
            var discount = this.Bind<Discount>();
            await _discountService.Update(discount);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteDiscount(dynamic parameters)
        {
            int discountId = parameters.id;
            await _discountService.Delete(discountId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
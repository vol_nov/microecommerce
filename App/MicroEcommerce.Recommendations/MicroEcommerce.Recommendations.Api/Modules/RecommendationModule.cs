﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.BusinessLayer;
using MicroEcommerce.Recommendations.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Recommendations.Api.Modules
{
    public class RecommendationModule : NancyModule
    {
        private readonly IRecommendationService _recommendationService;

        public RecommendationModule(IRecommendationService recommendationService) : base("/api/recommendations")
        {
            _recommendationService = recommendationService;

            Get("/recommendation", GetRecommendations);
            Get("/recommendation/{id}", GetRecommendationById);
            Get("/recommendation/rated", GetRecommendationsTopRated);
            Get("/recommendation/bought", GetRecommendationsTopBuyed);
            Get("/recommendation/discount", GetRecommendationsByDiscount);
            Get("/recommendation/category/similar", GetRecommendationsSimilarCategory);
            Get("/recommendation/category/connected", GetRecommendationsConnectedWithCategory);
            Post("/recommendation", AddRecommendation);
            Put("/recommendation", UpdateRecommendation);
            Delete("/recommendation/{id}", DeleteRecommendation);
        }

        public async Task<object> GetRecommendations(dynamic parameters)
        {
            var recommendations = await _recommendationService.GetModels();
            return Negotiate.WithModel(recommendations).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetRecommendationById(dynamic parameters)
        {
            var recommendation = await _recommendationService.GetModelById((int) parameters.id);
            return Negotiate.WithModel(recommendation).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddRecommendation(dynamic parameters)
        {
            var recommendation = this.Bind<Recommendation>();
            await _recommendationService.Add(recommendation);
            return Negotiate.WithModel(recommendation).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateRecommendation(dynamic parameters)
        {
            var recommendation = this.Bind<Recommendation>();
            await _recommendationService.Update(recommendation);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteRecommendation(dynamic parameters)
        {
            int recommendationId = parameters.id;
            await _recommendationService.Delete(recommendationId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetRecommendationsTopRated(dynamic parameters)
        {
            int userId = this.Request.Query.userId;
            int count = this.Request.Query.count;

            var recommendation = await _recommendationService.GetRecommendationsTopRated(userId, count);
            
            return Negotiate.WithModel(recommendation).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetRecommendationsTopBuyed(dynamic parameters)
        {
            int userId = this.Request.Query.userId;
            int count = this.Request.Query.count;

            var recommendation = await _recommendationService.GetRecommendationsTopBuyed(userId, count);
            
            return Negotiate.WithModel(recommendation).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetRecommendationsSimilarCategory(dynamic parameters)
        {
            int userId = this.Request.Query.userId;
            int count = this.Request.Query.count;
            Guid categoryId = this.Request.Query.categoryId;

            var recommendation =
                await _recommendationService.GetRecommendationsSimilarCategory(userId, categoryId, count);
            
            return Negotiate.WithModel(recommendation).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetRecommendationsConnectedWithCategory(dynamic parameters)
        {
            int userId = this.Request.Query.userId;
            int count = this.Request.Query.count;
            Guid categoryId = this.Request.Query.categoryId;

            var recommendation =
                await _recommendationService.GetRecommendationsConnectedWithCategory(userId, categoryId, count);
            
            return Negotiate.WithModel(recommendation).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetRecommendationsByDiscount(dynamic parameters)
        {
            int userId = this.Request.Query.userId;
            int count = this.Request.Query.count;
            
            var recommendation =
                await _recommendationService.GetRecommendationsByDiscount(userId, count);
            
            return Negotiate.WithModel(recommendation).WithStatusCode(HttpStatusCode.OK);
        }
    }
}
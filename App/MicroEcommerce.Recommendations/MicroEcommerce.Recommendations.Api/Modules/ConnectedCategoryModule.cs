﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.Recommendations.BusinessLayer;
using MicroEcommerce.Recommendations.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Recommendations.Api.Modules
{
    public class ConnectedCategoryModule : NancyModule
    {
        private readonly IConnectedCategoryService _connectedCategoryService;

        public ConnectedCategoryModule(IConnectedCategoryService connectedcategoryService) : base("/api/recommendations")
        {
            _connectedCategoryService = connectedcategoryService;

            Get("/connectedcategory", GetConnectedCategories);
            Get("/connectedcategory/{id}", GetConnectedCategoryById);
            Post("/connectedcategory", AddConnectedCategory);
            Put("/connectedcategory", UpdateConnectedCategory);
            Delete("/connectedcategory/{id}", DeleteConnectedCategory);
        }

        public async Task<object> GetConnectedCategories(dynamic parameters)
        {
            var categories = await _connectedCategoryService.GetModels();
            return Negotiate.WithModel(categories).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetConnectedCategoryById(dynamic parameters)
        {
            var connectedcategory = await _connectedCategoryService.GetModelById((int)parameters.id);
            return Negotiate.WithModel(connectedcategory).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddConnectedCategory(dynamic parameters)
        {
            var connectedCategory = this.Bind<ConnectedCategory>();
            await _connectedCategoryService.Add(connectedCategory);
            return Negotiate.WithModel(connectedCategory).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateConnectedCategory(dynamic parameters)
        {
            var connectedCategory = this.Bind<ConnectedCategory>();
            await _connectedCategoryService.Update(connectedCategory);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteConnectedCategory(dynamic parameters)
        {
            int connectedCategoryId = parameters.id;
            await _connectedCategoryService.Delete(connectedCategoryId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
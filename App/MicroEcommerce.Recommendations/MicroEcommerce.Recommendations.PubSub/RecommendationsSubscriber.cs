﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MicroEcommerce.PubSub;
using MicroEcommerce.Recommendations.BusinessLayer;
using MicroEcommerce.Recommendations.Models;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using Serilog;

namespace MicroEcommerce.Recommendations.PubSub
{
    public class RecommendationsSubscriber : ISubscriber
    {
        private readonly IReceiveExchange _receiveExchange;
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IDiscountService _discountService;
        private readonly ILogger _logger;

        public RecommendationsSubscriber(IReceiveExchange receiveExchange, 
            IProductService productService,
            ICategoryService categoryService,
            IDiscountService discountService,
            ILogger logger)
        {
            _receiveExchange = receiveExchange;
            _productService = productService;
            _categoryService = categoryService;
            _discountService = discountService;
            _logger = logger;
        }

        public async Task OnProductAdded(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);

            dynamic product = JsonConvert.DeserializeObject(jsonProduct);

            IEnumerable<dynamic> categories = product.Categories; 
            
            await _productService.Add(new Product()
            {
                ProductId = (Guid)product.ProductId,
                Price = (decimal)product.Price,
                ProductCategories = (categories != null) ? categories.Select(c => new ProductCategory()
                {
                    ProductId = (Guid)product.ProductId,
                    CategoryId = (Guid)c.CategoryId
                }).ToList() : null
            });

            _logger.Information($"Product added: {jsonProduct}");
        }
        
        public async Task OnProductDeleted(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);

            dynamic product = JsonConvert.DeserializeObject(jsonProduct);

            IEnumerable<dynamic> categories = product.Categories;

            Guid productId = product.ProductId;
            
            await _productService.Delete(productId);
            var deleteDiscounts = await _discountService.Where(d => d.ProductId == productId);
            Parallel.ForEach(deleteDiscounts, item => _discountService.Delete(item.DiscountId));
            _logger.Information($"Product added: {jsonProduct}");
        }

        public async Task OnProductLiked(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);

            dynamic product = JsonConvert.DeserializeObject(jsonProduct);

            var entity = await _productService.GetModelById((Guid) product.ProductId);
            entity.LikesCount++;
            await _productService.Update(entity);
        }
        
        public async Task OnProductBought(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);

            dynamic product = JsonConvert.DeserializeObject(jsonProduct);

            var entity = await _productService.GetModelById((Guid) product.ProductId);
            entity.BoughtCount++;
            await _productService.Update(entity);
            _logger.Information($"Product added: {jsonProduct}");
        }

        private async Task OnDiscountDeleted(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonDiscount = Encoding.UTF8.GetString(body);
            dynamic discount = JsonConvert.DeserializeObject(jsonDiscount);

            await _discountService.Delete((int)discount.DiscountId);
            _logger.Information($"Discount deleted: {jsonDiscount}");
        }

        private async Task OnDiscountAdded(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonDiscount = Encoding.UTF8.GetString(body);
            dynamic discount = JsonConvert.DeserializeObject(jsonDiscount);

            await _discountService.Add(new Discount()
            {
                DiscountId = (int)discount.DiscountId,
                ProductId = (Guid)discount.ProductId,
                Value = (decimal)discount.Value
            });
            _logger.Information($"Discount added: {jsonDiscount}");
        }

        private async Task OnCategoryDeleted(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonCategory = Encoding.UTF8.GetString(body);
            dynamic category = JsonConvert.DeserializeObject(jsonCategory);
            
            await _categoryService.Add(new Category()
            {
                CategoryId = (Guid)category.CategoryId
            });

            _logger.Information($"Category added: {jsonCategory}");
        }

        private async Task OnCategoryAdded(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonCategory = Encoding.UTF8.GetString(body);
            dynamic category = JsonConvert.DeserializeObject(jsonCategory);

            await _categoryService.Delete((Guid)category.CategoryId);

            _logger.Information($"Category added: {jsonCategory}");
        }
        
        public void RegisterExchanges()
        {
            _receiveExchange.Receive("productAdded", (sender, eventArgs) =>
            {
                OnProductAdded(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productDeleted", (sender, eventArgs) =>
            {
                OnProductDeleted(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productLiked", (sender, eventArgs) =>
            {
                OnProductLiked(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productBought", (sender, eventArgs) =>
            {
                OnProductBought(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("categoryAdded", (sender, eventArgs) =>
            {
                OnCategoryAdded(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("categoryDeleted", (sender, eventArgs) =>
            {
                OnCategoryDeleted(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("discountAdded", (sender, eventArgs) =>
            {
                OnDiscountAdded(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("discountDeleted", (sender, eventArgs) =>
            {
                OnDiscountDeleted(sender, eventArgs).Wait();
            });
        }
    }
}
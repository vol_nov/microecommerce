﻿using System;
using System.Text;
using MicroEcommerce.Logging;
using MicroEcommerce.PubSub;
using MicroEcommerce.Recommendations.BusinessLayer;
using MicroEcommerce.Recommendations.Infrastructure;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroEcommerce.Recommendations.PubSub
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Recommendations PubSub";
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            
            var configuration = builder.Build();
            var logger = LoggerConfig.Create();

            var container = new Container();
            var package = new Package();
            
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            container.Register(() => configuration);
            container.Register(() => logger);
            
            using (AsyncScopedLifestyle.BeginScope(container))
            {
                package.RegisterServices(container);

                var recommendationsPubSub = new RecommendationsSubscriber(
                    container.GetInstance<IReceiveExchange>(),
                    container.GetInstance<IProductService>(),
                    container.GetInstance<ICategoryService>(),
                    container.GetInstance<IDiscountService>(),
                    container.GetInstance<ILogger>());

                recommendationsPubSub.RegisterExchanges();

                Console.WriteLine("Recommendations PubSub is Up");

                Console.ReadLine();
            }
        }
    }
}
﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace MicroEcommerce.ApiGateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "ApiGateway microservice";
            BuildWebHost(args).Run();
         }
 
         public static IWebHost BuildWebHost(string[] args) =>
             WebHost.CreateDefaultBuilder(args)
                 .UseStartup<Startup>()
                 .UseUrls("http://localhost:5000")
                 .Build();
     }
 }
﻿using MicroEcommerce.SimpleInjector.Nancy;
using MicroEcommerce.UserStore.BusinessLayer;
using Microsoft.Extensions.Configuration;
using SimpleInjector.Packaging;

namespace MicroEcommerce.ApiGateway
{
    public class ApiGatewayBootstrapper : SimpleInjectorNancyBootstrapper
    {
        public ApiGatewayBootstrapper(IConfigurationRoot configurationRoot) : base(configurationRoot)
        {
            Packages = new IPackage[] { new MicroEcommerce.UserStore.Infrastructure.Package() };
        }

        public IUserService GetUserService()
        {
            return ApplicationContainer.Resolve<IUserService>();
        }
    }
}
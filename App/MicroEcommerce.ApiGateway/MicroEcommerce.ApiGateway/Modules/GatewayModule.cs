﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.Extensions.Configuration;
using Nancy;

namespace MicroEcommerce.ApiGateway.Modules
{
    public class GatewayModule : NancyModule
    {
        private IConfigurationSection _connectionSection;

        public GatewayModule(IConfigurationRoot config)
        {
            _connectionSection = config.GetSection("Connections");

            foreach (var microservice in _connectionSection.GetChildren())
            {
                var apiName = microservice.Key;
                var location = microservice.Value;

                Get($"/api/{apiName}/^(?:.*)", args =>
                {
                    return Response.AsRedirect(location + Request.Url.Path + Request.Url.Query);
                });
                Post($"/api/{apiName}/^(?:.*)", async args => 
                {
                    var client = new HttpClient();
                    using (var memoryStream = new MemoryStream())
                    {
                        client.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
                        
                        var contentType = this.Request.Headers.ContentType.ToString();
                        this.Request.Body.CopyTo(memoryStream);
                        var result = await client.PostAsync(location + this.Request.Url.Path + this.Request.Url.Query,
                            new StringContent(Encoding.UTF8.GetString(memoryStream.ToArray()), Encoding.UTF8,
                                contentType));
                        var content = await result.Content.ReadAsStringAsync();
                        return Response.AsJson(content);
                    }
                });
                Put($"/api/{apiName}/^(?:.*)", async args => 
                {
                    var client = new HttpClient();
                    using (var memoryStream = new MemoryStream())
                    {
                        client.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
                        
                        var contentType = this.Request.Headers.ContentType.ToString();
                        this.Request.Body.CopyTo(memoryStream);
                        var result = await client.PutAsync(location + this.Request.Url.Path + this.Request.Url.Query,
                            new StringContent(Encoding.UTF8.GetString(memoryStream.ToArray()), Encoding.UTF8,
                                contentType));
                        var content = await result.Content.ReadAsStringAsync();
                        return Response.AsJson(content);
                    }
                });
                Delete($"/api/{apiName}/^(?:.*)", async args => 
                {
                    var client = new HttpClient();
                    using (var memoryStream = new MemoryStream())
                    {
                        client.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
                        
                        var contentType = this.Request.Headers.ContentType.ToString();
                        this.Request.Body.CopyTo(memoryStream);
                        var result = await client.DeleteAsync(location + this.Request.Url.Path + this.Request.Url.Query);
                        var content = await result.Content.ReadAsStringAsync();
                        return Response.AsJson(content);
                    }
                });
            }
        }
    }
}
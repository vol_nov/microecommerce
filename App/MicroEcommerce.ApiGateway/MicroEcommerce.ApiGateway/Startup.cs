﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Logging;
using MicroEcommerce.UserStore.BusinessLayer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nancy.Owin;
using Serilog;
using Serilog.Events;
using MicroEcommerce.Auth;
using ILogger = Serilog.ILogger;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroEcommerce.ApiGateway
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        private Container _container;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()                
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            _container = new Container();
            _container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            var userStoreDependencies = new UserStore.Infrastructure.Package();
            _container.Register(() => Configuration, Lifestyle.Singleton);
            userStoreDependencies.RegisterServices(_container);
            _container.Verify();
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            using (AsyncScopedLifestyle.BeginScope(_container))
            {
                app.UseOwin(x => x.UseMonitoringAndLogging(ConfigureLogger(), HealthCheck)
                    .UseAuth(ConfigureLogger(), _container.GetInstance<IUserService>())
                    .UseNancy(new NancyOptions() { Bootstrapper = new ApiGatewayBootstrapper(Configuration) }));
            }
        }
        
        private ILogger ConfigureLogger()
        {
            return new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.ColoredConsole(
                    LogEventLevel.Verbose,
                    "{NewLine}{Timestamp:HH:mm:ss} [{Level}] ({CorrelationToken}) {Message}{NewLine}{Exception}")
                .CreateLogger();
        }
        
        private static Task<bool> HealthCheck()
        {
            return Task.FromResult(true);
        }
    }
}
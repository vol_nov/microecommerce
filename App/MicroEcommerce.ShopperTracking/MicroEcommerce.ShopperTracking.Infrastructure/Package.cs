﻿using System;
using MicroEcommerce.PubSub;
using MicroEcommerce.RepositoryBase.EFCore;
using MicroEcommerce.ShopperTracking.BusinessLayer;
using MicroEcommerce.ShopperTracking.Models;
using MicroEcommerce.ShopperTracking.Persistence;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace MicroEcommerce.ShopperTracking.Infrastructure
{
    public class Package : IPackage
    {
        public void RegisterServices(Container container)
        {
            RegisterBusinessLayerDependencies(container);
            RegisterPersistenceDependencies(container);
        }

        public void RegisterBusinessLayerDependencies(Container container)
        {
            container.Register<ITrackingService, TrackingService>(Lifestyle.Scoped);
        }

        public void RegisterPersistenceDependencies(Container container)
        {
            container.Register(() =>
            {
                var connectionString =
                    container.GetInstance<IConfigurationRoot>().GetConnectionString("DefaultConnectionString");
            
                return new TrackingContext(connectionString);
            }, Lifestyle.Scoped);
            
            container.Register<IRepository<Tracking, long>, TrackingRepository>(Lifestyle.Scoped);
            container.Register<ISendExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new SendExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);

            container.Register<IReceiveExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new ReceiveExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);
        }
    }
}
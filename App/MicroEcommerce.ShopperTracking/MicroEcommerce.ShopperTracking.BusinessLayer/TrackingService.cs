﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.RepositoryBase.EFCore;
using MicroEcommerce.ShopperTracking.Models;

namespace MicroEcommerce.ShopperTracking.BusinessLayer
{
    public interface ITrackingService
    {
        Task<Tracking> Add(Tracking tracking);
        Task Update(Tracking tracking);
        Task Remove(long trackingId);
        Task<Tracking> GetTrackingById(long trackingId);
        Task<ICollection<Tracking>> GetTrackings();
        Task<ICollection<Tracking>> GetTrackings(Guid productId);
        Task<ICollection<Tracking>> GetTrackings(Guid productId, int userId);
        Task<ICollection<Tracking>> GetTrackingsByCategory(Guid categoryId);
        Task<ICollection<Tracking>> GetTrackingsByActionAndUser(int userId, TrackingAction action);
        Task<ICollection<Tracking>> GetTrackingsByActionsAndUser(int userId, IEnumerable<TrackingAction> action);
        Task<ICollection<Tracking>> GetTrackingsByActionAndProduct(Guid productId, TrackingAction action);
        Task<ICollection<Tracking>> GetTrackingsByActionsAndProduct(Guid productId, IEnumerable<TrackingAction> action);
    }
    
    public class TrackingService : ITrackingService
    {
        private readonly IRepository<Tracking, long> _trackingRepository;

        public TrackingService(IRepository<Tracking, long> trackingRepository)
        {
            _trackingRepository = trackingRepository;
        }
        
        public async Task<Tracking> Add(Tracking tracking)
        {
            return await _trackingRepository.Add(tracking);
        }

        public async Task Update(Tracking tracking)
        {
            await _trackingRepository.Update(tracking);
        }

        public async Task Remove(long trackingId)
        {
            await _trackingRepository.Delete(trackingId);
        }

        public async Task<Tracking> GetTrackingById(long trackingId)
        {
            return await _trackingRepository.GetEntityById(trackingId);
        }

        public async Task<ICollection<Tracking>> GetTrackings()
        {
            return await _trackingRepository.GetEntities();
        }

        public async Task<ICollection<Tracking>> GetTrackings(Guid productId)
        {
            return await _trackingRepository.Where(t => t.ProductId == productId);
        }

        public async Task<ICollection<Tracking>> GetTrackings(Guid productId, int userId)
        {
            return await _trackingRepository.Where(t => t.ProductId == productId && t.UserId == userId);
        }

        public async Task<ICollection<Tracking>> GetTrackingsByCategory(Guid categoryId)
        {
            return await _trackingRepository.Where(t => t.Categories != null && t.Categories.Any(c => c.CategoryId == categoryId));
        }

        public async Task<ICollection<Tracking>> GetTrackingsByActionAndUser(int userId, TrackingAction action)
        {
            return await _trackingRepository.Where(t => t.UserId == userId && t.Action == action);
        }

        public async Task<ICollection<Tracking>> GetTrackingsByActionsAndUser(int userId, IEnumerable<TrackingAction> action)
        {
            return await _trackingRepository.Where(t => t.UserId == userId && action.Contains(t.Action));
        }

        public async Task<ICollection<Tracking>> GetTrackingsByActionAndProduct(Guid productId, TrackingAction action)
        {
            return await _trackingRepository.Where(t => t.ProductId == productId && t.Action == action);
        }

        public async Task<ICollection<Tracking>> GetTrackingsByActionsAndProduct(Guid productId, IEnumerable<TrackingAction> action)
        {
            return await _trackingRepository.Where(t => t.ProductId == productId && action.Contains(t.Action));
        }
    }
}
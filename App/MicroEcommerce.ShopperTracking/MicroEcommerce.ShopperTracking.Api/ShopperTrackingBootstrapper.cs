﻿using MicroEcommerce.ShopperTracking.Infrastructure;
using MicroEcommerce.SimpleInjector.Nancy;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace MicroEcommerce.ShopperTracking.Api
{
    public class ShopperTrackingBootstrapper : SimpleInjectorNancyBootstrapper
    {
        public ShopperTrackingBootstrapper(IConfigurationRoot configurationRoot, ILogger logger) : base(configurationRoot, logger)
        {
            Packages = new[] {new Package()};
        }
    }
}
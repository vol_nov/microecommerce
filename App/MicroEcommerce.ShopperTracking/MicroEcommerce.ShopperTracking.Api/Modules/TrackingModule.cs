﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.ShopperTracking.BusinessLayer;
using MicroEcommerce.ShopperTracking.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.ShopperTracking.Modules
{
    public class TrackingModule : NancyModule
    {
        private readonly ITrackingService _trackingService;

        public TrackingModule(ITrackingService trackingService) : base("/api/track")
        {
            _trackingService = trackingService;
            
            Get("/tracking", GetTrackings);
            Get("/tracking/{id}", GetTrackingById);
            Get("/tracking/product/{productId}/action/{action}", GetTrackingsByActionAndProduct);
            Get("/tracking/user/{userId}/action/{action}", GetTrackingsByActionAndUser);
            Get("/tracking/product/{productId}/actions/{actions}", GetTrackingsByActionsAndProduct);
            Get("/tracking/user/{userId}/actions/{actions}", GetTrackingsByActionsAndUser);
            Get("/tracking/category/{categoryId}", GetTrackingsByCategory);
            Post("/tracking", AddTracking);
            Put("/tracking", UpdateTracking);
            Delete("/tracking/{id}", RemoveTracking);
        }
        
        public async Task<object> AddTracking(dynamic args)
        {
            var tracking = this.Bind<Tracking>();
            tracking = await _trackingService.Add(tracking);
            return Negotiate.WithModel(tracking).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateTracking(dynamic args)
        {
            var tracking = this.Bind<Tracking>();
            await _trackingService.Update(tracking);
            return Negotiate.WithModel(tracking).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> RemoveTracking(dynamic args)
        {
            long trackingId = args.trackingId;
            await _trackingService.Remove(trackingId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetTrackingById(dynamic args)
        {
            long trackingId = args.trackingId;
            var tracking = await _trackingService.GetTrackingById((long)args.id);
            return Negotiate.WithModel(tracking).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetTrackings(dynamic args)
        {
            var trackings = await _trackingService.GetTrackings();
            return Negotiate.WithModel(trackings).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetTrackingsByCategory(dynamic args)
        {
            var trackings = await _trackingService.GetTrackingsByCategory((Guid)args.categoryId);
            return Negotiate.WithModel(trackings).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetTrackingsByActionAndUser(dynamic args)
        {
            var trackings = await _trackingService.GetTrackingsByActionAndUser((int)args.userId, (TrackingAction)((int)args.action));
            return Negotiate.WithModel(trackings).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetTrackingsByActionsAndUser(dynamic args)
        {
            var trackings = await _trackingService.GetTrackingsByActionsAndUser((int)args.userId, ParseActions((string)args.actions));
            return Negotiate.WithModel(trackings).WithStatusCode(HttpStatusCode.OK);  
        }

        public async Task<object> GetTrackingsByActionAndProduct(dynamic args)
        {
            var trackings = await _trackingService.GetTrackingsByActionAndProduct((Guid)args.productId, (TrackingAction)((int)args.action));
            return Negotiate.WithModel(trackings).WithStatusCode(HttpStatusCode.OK);   
        }

        public async Task<object> GetTrackingsByActionsAndProduct(dynamic args)
        {
            var trackings = await _trackingService.GetTrackingsByActionsAndProduct((Guid)args.productId, ParseActions((string)args.actions));
            return Negotiate.WithModel(trackings).WithStatusCode(HttpStatusCode.OK);  
        }

        private IEnumerable<TrackingAction> ParseActions(string actions)
        {
            var chars = actions.ToCharArray();
            var actionsList = new List<TrackingAction>();

            foreach (var c in chars)
            {
                actionsList.Add(Enum.Parse<TrackingAction>(Convert.ToString(c)));
            }

            return actionsList;
        }
    }
}
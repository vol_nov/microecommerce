﻿using System;

namespace MicroEcommerce.ShopperTracking.Models
{    
    [Serializable]
    public class TrackingCategory
    {
        public long TrackingCategoryId { get; set;}
        public long TrackingId { get; set; }
        public Guid CategoryId { get; set; }
    }
}
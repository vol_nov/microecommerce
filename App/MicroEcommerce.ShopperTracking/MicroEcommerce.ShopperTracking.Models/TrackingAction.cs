﻿using System;

namespace MicroEcommerce.ShopperTracking.Models
{
    [Serializable]
    public enum TrackingAction
    {
        Created = 0, Deleted, Bought, Delivered, Liked, Commented
    }   
}
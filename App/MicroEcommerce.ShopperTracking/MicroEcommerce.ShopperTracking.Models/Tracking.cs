﻿using System;
using System.Collections.Generic;

namespace MicroEcommerce.ShopperTracking.Models
{
    [Serializable]
    public class Tracking
    {
        public long TrackingId { get; set; }
        public Guid ProductId { get; set; }
        public ICollection<TrackingCategory> Categories { get; set; }
        public decimal TotalPrice { get; set; }
        public int? UserId { get; set; }
        public TrackingAction Action { get; set; }
        public DateTime TrackDateTime { get; set; }
    }

}
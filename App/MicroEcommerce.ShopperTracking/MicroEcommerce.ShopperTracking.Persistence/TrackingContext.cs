﻿using System.ComponentModel.DataAnnotations.Schema;
using MicroEcommerce.ShopperTracking.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace MicroEcommerce.ShopperTracking.Persistence
{
    public class TrackingContext : DbContext, IDesignTimeDbContextFactory<TrackingContext>
    {
        private readonly string _connectionString;
        
        public DbSet<Tracking> Trackings { get; set; }
        public DbSet<TrackingCategory> TrackingCategories { get; set; }

        public TrackingContext()
        {
            
        }
        
        public TrackingContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        public TrackingContext CreateDbContext(string[] args)
        {
            return new TrackingContext("Server=NOTEBOOK;Database=MicroEcommerce.ShopperTracking;Trusted_Connection=True;");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tracking>().HasKey(pp => pp.TrackingId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<TrackingCategory>().HasKey(c => c.TrackingCategoryId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MicroEcommerce.RepositoryBase.EFCore;
using MicroEcommerce.ShopperTracking.Models;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.ShopperTracking.Persistence
{
    public class TrackingRepository : Repository<Tracking, long, TrackingContext>
    {
        public TrackingRepository(TrackingContext context) : base(context)
        {
        }

        protected override DbSet<Tracking> Table => Context.Trackings;
        protected override IQueryable<Tracking> Include(DbSet<Tracking> table)
        {
            return Table.Include(t => t.Categories).AsNoTracking();
        }

        public override async Task<Tracking> GetEntityById(long id)
        {
            return await Table.FirstOrDefaultAsync(t => t.TrackingId == id);
        }
    }
}
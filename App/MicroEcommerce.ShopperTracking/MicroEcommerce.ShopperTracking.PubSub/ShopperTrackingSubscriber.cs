﻿using System;
using System.Text;
using System.Threading.Tasks;
using MicroEcommerce.PubSub;
using MicroEcommerce.ShopperTracking.BusinessLayer;
using MicroEcommerce.ShopperTracking.Models;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using Serilog;

namespace MicroEcommerce.ShopperTracking.PubSub
{
    public class ShopperTrackingSubscriber : ISubscriber
    {
        private readonly IReceiveExchange _receiveExchange;
        private readonly ITrackingService _trackingService;
        private readonly ILogger _logger;

        public ShopperTrackingSubscriber(IReceiveExchange receiveExchange, 
            ITrackingService trackingService,
            ILogger logger)
        {
            _receiveExchange = receiveExchange;
            _trackingService = trackingService;
            _logger = logger;
        }

        public async Task OnProductAdded(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);
            dynamic product = JsonConvert.DeserializeObject(jsonProduct);
            
            await _trackingService.Add(new Tracking() { Action = TrackingAction.Created, 
                TrackDateTime = DateTime.Now,
                ProductId = (Guid)product.ProductId,
                UserId = (int)product.UserId,
                TotalPrice = (decimal)product.Price * (decimal)(double)product.Count
            });

            _logger.Information($"Product added: {jsonProduct}");
        }

        public async Task OnProductCommented(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);
            dynamic product = JsonConvert.DeserializeObject(jsonProduct);

            await _trackingService.Add(new Tracking() { Action = TrackingAction.Commented, 
                TrackDateTime = DateTime.Now,
                ProductId = (Guid)product.ProductId,
                UserId = (int)product.UserId,
                TotalPrice = (decimal)product.Price
            });

            _logger.Information($"Product commented: {jsonProduct}");
        }
        
        public async Task OnProductLiked(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);
            dynamic product = JsonConvert.DeserializeObject(jsonProduct);

            await _trackingService.Add(new Tracking() { Action = TrackingAction.Liked, 
                TrackDateTime = DateTime.Now,
                ProductId = (Guid)product.ProductId,
                UserId = (int)product.UserId,
                TotalPrice = (decimal)product.Price
            });
            
            _logger.Information($"Product liked: {jsonProduct}");
        }
        
        public async Task OnProductBought(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);
            dynamic product = JsonConvert.DeserializeObject(jsonProduct);

            await _trackingService.Add(new Tracking() { Action = TrackingAction.Bought, 
                TrackDateTime = DateTime.Now,
                ProductId = (Guid)product.ProductId,
                UserId = (int)product.UserId,
                TotalPrice = (decimal)product.Price
            });
            
            _logger.Information($"Product bought: {jsonProduct}");
        }

        public void RegisterExchanges()
        {
            _receiveExchange.Receive("productAdded", (sender, eventArgs) =>
            {
                OnProductAdded(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productCommented", (sender, eventArgs) =>
            {
                OnProductCommented(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productLiked", (sender, eventArgs) =>
            {
                OnProductLiked(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productBought", (sender, eventArgs) =>
            {
                OnProductBought(sender, eventArgs).Wait();
            });
        }
    }
}
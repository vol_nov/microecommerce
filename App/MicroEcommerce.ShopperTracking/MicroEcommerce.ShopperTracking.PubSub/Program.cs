﻿using System;
using MicroEcommerce.Logging;
using MicroEcommerce.PubSub;
using MicroEcommerce.ShopperTracking.BusinessLayer;
using MicroEcommerce.ShopperTracking.Infrastructure;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroEcommerce.ShopperTracking.PubSub
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "ShopperTracking PubSub";
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            
            var configuration = builder.Build();
            var logger = LoggerConfig.Create();

            var container = new Container();
            var package = new Package();
            
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            container.Register(() => configuration);
            container.Register(() => logger);

            using (AsyncScopedLifestyle.BeginScope(container))
            {
                package.RegisterServices(container);

                var shopperTrackingPubSub = new ShopperTrackingSubscriber(
                    container.GetInstance<IReceiveExchange>(),
                    container.GetInstance<ITrackingService>(),
                    container.GetInstance<ILogger>());

                shopperTrackingPubSub.RegisterExchanges();

                Console.WriteLine("ShopperTracking PubSub is Up");

                Console.ReadLine();
            }
        }
    }
}
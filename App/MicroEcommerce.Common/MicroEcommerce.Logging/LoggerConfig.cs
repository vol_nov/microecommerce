﻿using Serilog;
using Serilog.Events;

namespace MicroEcommerce.Logging
{
    public static class LoggerConfig
    {
        public static ILogger Create()
        {
            return new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.ColoredConsole(
                    LogEventLevel.Verbose,
                    "{NewLine}{Timestamp:HH:mm:ss} [{Level}] ({CorrelationToken}) {Message}{NewLine}{Exception}")
                .CreateLogger();
        }
    }
}

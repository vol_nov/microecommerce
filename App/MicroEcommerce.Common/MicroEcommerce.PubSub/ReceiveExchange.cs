﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Serilog;

namespace MicroEcommerce.PubSub
{
    public interface IReceiveExchange
    {
        void Receive(string exchange, EventHandler<BasicDeliverEventArgs> eventHandler);
    }
    
    public class ReceiveExchange : IReceiveExchange, IDisposable
    {
        protected ConnectionFactory Factory { get; set; } 
        protected IConnection Connection { get; set; }
        protected IModel Channel { get; set; }
        protected ILogger Logger { get; set; }

        public ReceiveExchange(string hostName, ILogger logger)
        {
            Factory = new ConnectionFactory() { HostName = hostName };
            Connection = Factory.CreateConnection();
            Channel = Connection.CreateModel();
        }

        public void Receive(string exchange, EventHandler<BasicDeliverEventArgs> eventHandler)
        {
            try
            {
                Channel.ExchangeDeclare(exchange: exchange, type: "fanout");

                var queueName = Channel.QueueDeclare().QueueName;
                Channel.QueueBind(queue: queueName,
                    exchange: exchange,
                    routingKey: "");

                var consumer = new EventingBasicConsumer(Channel);
                consumer.Received += eventHandler;

                Channel.BasicConsume(queue: queueName,
                    autoAck: true,
                    consumer: consumer);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"{e.Message}");
            }
        }

        public void Dispose()
        {
            Connection?.Dispose();
            Channel?.Dispose();
        }
    }
}
﻿using System;
using System.Text;
using RabbitMQ.Client;
using Serilog;
using Serilog.Core;

namespace MicroEcommerce.PubSub
{
    public interface ISendExchange
    {
        void Send(string exchange, string message);
    }
    
    public class SendExchange : ISendExchange, IDisposable
    {
        protected ConnectionFactory Factory { get; set; } 
        protected IConnection Connection { get; set; }
        protected IModel Channel { get; set; }
        protected ILogger Logger { get; set; }

        public SendExchange(string hostName, ILogger logger)
        {
            Factory = new ConnectionFactory() { HostName = hostName };
            Connection = Factory.CreateConnection();
            Channel = Connection.CreateModel();
            Logger = logger;
        }

        public void Send(string exchange, string message)
        {
            try
            {
                Channel.ExchangeDeclare(exchange: exchange, type: "fanout");

                var body = Encoding.UTF8.GetBytes(message);
                Channel.BasicPublish(exchange: exchange,
                    routingKey: "",
                    basicProperties: null,
                    body: body);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"{e.Message}");
            }
        }

        public void Dispose()
        {
            Connection?.Dispose();
            Channel?.Dispose();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroEcommerce.PubSub
{
    public interface ISubscriber
    {
        void RegisterExchanges();
    }
}

﻿using System.Collections.Generic;
using MicroEcommerce.Platform;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Configuration;
using Nancy.TinyIoc;
using Serilog;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using SimpleInjector.Packaging;

namespace MicroEcommerce.SimpleInjector.Nancy
{
    public class SimpleInjectorNancyBootstrapper: DefaultNancyBootstrapper
    {
        private readonly IConfigurationRoot _configurationRoot;
        protected ILogger Logger;
        protected IEnumerable<IPackage> Packages;

        protected SimpleInjectorNancyBootstrapper(IConfigurationRoot configurationRoot, ILogger logger)
        {
            _configurationRoot = configurationRoot;
            Logger = logger;
        }
        
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            
            var simpleInjectorContainer = new Container();
            
            simpleInjectorContainer.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            simpleInjectorContainer.Register(() => _configurationRoot, Lifestyle.Singleton);
            simpleInjectorContainer.Register(() => Logger, Lifestyle.Singleton);

            if (Packages != null)
            {
                foreach (var package in Packages)
                {
                    package.RegisterServices(simpleInjectorContainer);
                }
            }
            
            foreach (var nancyModule in Modules)
            {
                simpleInjectorContainer.Register(nancyModule.ModuleType);
            }
            
            simpleInjectorContainer.Verify();

            container.Register(typeof(INancyModuleCatalog), new SimpleInjectorModuleCatalog(simpleInjectorContainer));
            container.Register(typeof(INancyContextFactory), new SimpleInjectorScopedContextFactory(
                simpleInjectorContainer, container.Resolve<INancyContextFactory>()));
        }

        public override void Configure(INancyEnvironment environment)
        {
            var config = new TraceConfiguration(enabled: true, displayErrorTraces: true);
            environment.AddValue(config);
        }
    }
}
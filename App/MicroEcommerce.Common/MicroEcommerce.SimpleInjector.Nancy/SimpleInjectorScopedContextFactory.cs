﻿using Nancy;
using SimpleInjector;
using Lifestyles = SimpleInjector.Lifestyles;

namespace MicroEcommerce.SimpleInjector.Nancy
{
    public sealed class SimpleInjectorScopedContextFactory : INancyContextFactory
    {
        private readonly Container _container;
        private readonly INancyContextFactory _defaultFactory;

        public SimpleInjectorScopedContextFactory(Container container, INancyContextFactory @default)
        {
            _container = container;
            _defaultFactory = @default;
        }

        public NancyContext Create(Request request)
        {
            var context = _defaultFactory.Create(request);
            context.Items.Add("SimpleInjector.Scope", Lifestyles.AsyncScopedLifestyle.BeginScope(_container));
            return context;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.RepositoryBase.EFCore
{
    public abstract class Repository<T, TIdentity, TDbContext> : IRepository<T, TIdentity> 
                                                                     where T : class, new()
                                                                     where TDbContext : DbContext
    {
        protected readonly TDbContext Context;
        protected abstract DbSet<T> Table { get; }
        
        protected Repository(TDbContext context)
        {
            Context = context;
        }

        protected abstract IQueryable<T> Include(DbSet<T> table);
        
        public virtual async Task<ICollection<T>> GetEntities()
        {
            return await Include(Table).AsNoTracking().ToListAsync();
        }

        public abstract Task<T> GetEntityById(TIdentity id);

        public virtual async Task Delete(TIdentity id)
        {
            var entity = await GetEntityById(id);
            Table.Remove(entity);
            await Context.SaveChangesAsync();
        }

        public virtual async Task<T> Add(T entity)
        {
            var entityEntry = await Table.AddAsync(entity);
            await Context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
            return entity;
        }

        public virtual async Task Update(T entity)
        {
            var entityEntry = Table.Update(entity);
            await Context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
        }

        public virtual async Task<ICollection<T>> Where(Expression<Func<T, bool>> condition)
        {
            return await Include(Table).Where(condition).ToListAsync();
        }

        public async Task<ICollection<T>> OrderBy<TSelect>(Expression<Func<T, TSelect>> selector)
        {
            return await Include(Table).OrderBy(selector).ToListAsync();
        }

        public async Task<ICollection<T>> OrderByDescending<TSelect>(Expression<Func<T, TSelect>> selector)
        {
            return await Include(Table).OrderByDescending(selector).ToListAsync();
        }

        public IQueryable<T> WhereQueryable(Expression<Func<T, bool>> condition)
        {
            return Include(Table).Where(condition);
        }
        
        public IQueryable<T> OrderByQueryable<TSelect>(Expression<Func<T, TSelect>> selector)
        {
            return Include(Table).OrderBy(selector);
        }

        public IQueryable<T> OrderByDescendingQueryable<TSelect>(Expression<Func<T, TSelect>> selector)
        {
            return Include(Table).OrderByDescending(selector);
        }
        
        public virtual async Task<T> FirstOrDefault(Expression<Func<T, bool>> condition)
        {
            return await Include(Table).SingleOrDefaultAsync(condition);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MicroEcommerce.RepositoryBase.EFCore
{
    public interface IRepository<T, TIdentity> where T : class, new()
    {
        Task<ICollection<T>> GetEntities();
        Task<T> GetEntityById(TIdentity id);
        Task<T> Add(T entity);
        Task Update(T entity);
        Task Delete(TIdentity id);
        Task<ICollection<T>> Where(Expression<Func<T, bool>> condition);
        Task<ICollection<T>> OrderBy<TSelect>(Expression<Func<T, TSelect>> selector);
        Task<ICollection<T>> OrderByDescending<TSelect>(Expression<Func<T, TSelect>> selector);
        Task<T> FirstOrDefault(Expression<Func<T, bool>> condition);
        IQueryable<T> WhereQueryable(Expression<Func<T, bool>> condition);
        IQueryable<T> OrderByQueryable<TSelect>(Expression<Func<T, TSelect>> selector);
        IQueryable<T> OrderByDescendingQueryable<TSelect>(Expression<Func<T, TSelect>> selector);
    }
}
﻿using LibOwin;
using MicroEcommerce.UserStore.BusinessLayer;
using Serilog;
using System.Threading.Tasks;

namespace MicroEcommerce.Auth
{
    using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;

    public class Authentication
    {
        public static AppFunc Middleware(AppFunc next, ILogger log, IUserService userService)
        {
            return async env =>
            {
                var owinContext = new OwinContext(env);
                if (owinContext.Request.Path.Value == "/api/userstore/users" && owinContext.Request.Method == "POST")
                {
                    await next(env);
                }
                else
                {
                    if (owinContext.Request.Headers.ContainsKey("Email") && owinContext.Request.Headers.ContainsKey("Password")
                        && await userService.SignIn(owinContext.Request.Headers["Email"], owinContext.Request.Headers["Password"]))
                    {
                        await next(env);
                    }
                    else
                    {
                        owinContext.Response.StatusCode = 403;
                        await Task.FromResult(0);
                    }
                }
            };
        }
    }
}

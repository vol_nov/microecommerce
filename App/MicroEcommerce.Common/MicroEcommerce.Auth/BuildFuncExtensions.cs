﻿using MicroEcommerce.UserStore.BusinessLayer;
using Serilog;
using BuildFunc = System.Action<System.Func<
   System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>,
   System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>>>;

namespace MicroEcommerce.Auth
{
    public static class BuildFuncExtensions
    {
        public static BuildFunc UseAuth(this BuildFunc buildFunc, ILogger logger, IUserService userService)
        {
            buildFunc(next => Authentication.Middleware(next, logger, userService));
            return buildFunc;
        }
    }
}

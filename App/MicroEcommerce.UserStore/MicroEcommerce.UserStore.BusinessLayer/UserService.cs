﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.UserStore.Models;
using MicroEcommerce.UserStore.Persistence;
using Microsoft.Extensions.Configuration;

namespace MicroEcommerce.UserStore.BusinessLayer
{
    public interface IUserService
    {
        Task<int> Register(User user);
        Task<bool> SignIn(string email, string password);
        Task<User> GetUser(int id);
        Task Remove(int id);
        Task Update(User user);
    }
    
    public class UserService: IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        
        public async Task<int> Register(User user)
        {
            return await _userRepository.Add(user);
        }

        public async Task<bool> SignIn(string email, string password)
        {
            return await _userRepository.SignIn(email, password);
        }

        public async Task<User> GetUser(int id)
        {
            return await _userRepository.GetUserById(id);
        }

        public async Task Remove(int id)
        {
            await _userRepository.Delete(id);
        }

        public async Task Update(User user)
        {
            await _userRepository.Update(user);
        }
    }
}
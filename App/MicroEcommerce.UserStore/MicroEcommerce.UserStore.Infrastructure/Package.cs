﻿using System;
using MicroEcommerce.UserStore.BusinessLayer;
using MicroEcommerce.UserStore.Persistence;
using Microsoft.Extensions.Configuration;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace MicroEcommerce.UserStore.Infrastructure
{
    public class Package: IPackage
    {
        public void RegisterServices(Container container)
        {
            RegisterPersistenceDependencies(container);
            RegisterBusinessLayerDependencies(container);
        }

        private void RegisterBusinessLayerDependencies(Container container)
        {
            container.Register<IUserService, UserService>(Lifestyle.Scoped);
        }

        private void RegisterPersistenceDependencies(Container container)
        {
            container.Register<IUserRepository>(() => new UserRepository(GetConnectionString(container)), Lifestyle.Scoped);
        }

        private string GetConnectionString(Container container)
        {
            return container.GetInstance<IConfigurationRoot>().GetConnectionString("DefaultConnectionString");
        }
    }
}
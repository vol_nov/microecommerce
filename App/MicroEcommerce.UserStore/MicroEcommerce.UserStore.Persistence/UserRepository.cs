﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Dapper;
using MicroEcommerce.UserStore.Models;

namespace MicroEcommerce.UserStore.Persistence
{
    public class UserRepository: IUserRepository
    {
        private readonly string _connectionString;
           
        private readonly string _selectUsers =
            @"SELECT [User].[UserId], [User].[Email], [User].[Password], 
                     [UserDetails].[FullName], [UserDetails].[DateOfBirth], 
                     [Country].[CountryId], [Country].[Name] as CountryName, [City].[CityId], [City].[Name] as CityName 
FROM [User] 
LEFT JOIN [UserDetails] ON [User].[UserId] = [UserDetails].[UserId]
LEFT JOIN [Country] ON [Country].[CountryId] = [UserDetails].[CountryId]
LEFT JOIN [City] ON [City].[CityId] = [UserDetails].[CityId]";

        public UserRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        private User SelectUser(User user, UserDetails userDetails, Country country, City city)
        {
            user.UserDetails = userDetails;
            if (userDetails != null)
            {
                userDetails.Country = country;
                userDetails.City = city;
            }
            return user;
        }
        
        public async Task<int> Add(User user)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                user.UserId = await connection.QueryFirstOrDefaultAsync<int>(@"INSERT INTO [User]([Email], [Password]) 
VALUES (@Email, @Password); 
SELECT CAST(SCOPE_IDENTITY() as int)", 
                    user);

                if (user.UserDetails != null)
                {
                    user.UserDetails.UserId = user.UserId;
                    await connection.ExecuteAsync(
                        @"INSERT INTO [UserDetails]([UserId], [FullName], [CountryId], [CityId], [DateOfBirth]) 
VALUES (@UserId, @FullName, @CountryId, @CityId, @DateOfBirth)",
                        new { UserId = user.UserDetails.UserId, 
                            FullName = user.UserDetails.FullName, 
                            CountryId = user.UserDetails.Country?.CountryId, 
                            CityId = user.UserDetails.City?.CityId, 
                            DateOfBirth = user.UserDetails.DateOfBirth 
                        });
                }
            }
            return user.UserId;
        }

        public async Task Update(User user)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"UPDATE [User] 
SET [Email] = @Email, [Password] = @Password 
WHERE [Id] = @Id", user);
                if (user.UserDetails != null)
                   await connection.ExecuteAsync(@"UPDATE [UserDetails] 
SET [FullName] = @FullName, [CountryId] = @CountryId, [CityId] = @CityId, [DateOfBirth] = @DateOfBirth 
WHERE [UserId] = @UserId", user.UserDetails);
            }
        }

        public async Task Delete(User user)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"DELETE FROM [User] WHERE [UserId] = @Id", user);
                if (user.UserDetails != null)
                    await connection.ExecuteAsync(@"DELETE FROM [UserDetails] WHERE [UserId] = @Id", user.UserDetails);
            }
        }

        public async Task Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"DELETE FROM [User] WHERE [UserId] = @UserId", new { Id = id});
                await connection.ExecuteAsync(@"DELETE FROM [UserDetails] WHERE [UserId] = @UserId", new { Id = id });
            }
        }

        public async Task<User> GetUserById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                dynamic dynUser = connection.QueryFirstOrDefault(_selectUsers + " WHERE [User].[UserId] = @UserId", new { UserId = id });
                return ConvertDynamicToUser(dynUser);
            }
        }

        private User ConvertDynamicToUser(dynamic dynUser)
        {
            if (dynUser == null)
                return null;
            
            User user = new User((int)dynUser.UserId, (string)dynUser.Email, (string)dynUser.Password);
            
            user.UserDetails = new UserDetails();
            user.UserDetails.UserId = dynUser.UserId;
            user.UserDetails.DateOfBirth = dynUser.DateOfBirth == null ? DateTime.MinValue : dynUser.DateOfBirth;
            user.UserDetails.FullName = dynUser.FullName;
            
            if (dynUser.CountryId != null)
            {
                user.UserDetails.Country = new Country();
                user.UserDetails.Country.CountryId = (dynUser.CountryId == null ? -1 : (int) dynUser.CountryId);
                user.UserDetails.Country.Name = dynUser.CountryName;
            }
            
            if (dynUser.CityId != null)
            {
                user.UserDetails.City = new City();
                user.UserDetails.City.CityId = (dynUser.CityId == null ? -1 : (int) dynUser.CityId);
                user.UserDetails.City.Name = dynUser.CityName;
            }
            return user;
        }

        public async Task<User> GetUsersByEmail(string email)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return await connection.QueryFirstOrDefault(_selectUsers + " WHERE [User].[Email] = @Email", new { Email = email });
            }
        }

        public Task<IEnumerable<User>> GetAllUsers()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.QueryAsync<User, UserDetails, Country, City, User>(_selectUsers, SelectUser);
            }
        }

        public async Task<bool> SignIn(string email, string password)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return await connection.QueryFirstOrDefaultAsync<bool>("SELECT (CASE WHEN COUNT(*) = 1 THEN 1 ELSE 0 END) FROM [User] WHERE [Email] = @Email AND [Password] = @Password", 
                    new { Email = email, Password = password });
            }
        }
    }
}
USE [MicroEcommerce.UserStore]
GO

CREATE TABLE [dbo].[Country]
(
  [CountryId] TINYINT PRIMARY KEY IDENTITY(1, 1),
  [Name] VARCHAR(64) UNIQUE NOT NULL
)
GO

CREATE TABLE [dbo].[City]
(
  [CityId] INT PRIMARY KEY IDENTITY (1, 1),
  [Name] VARCHAR(128) NOT NULL,
  [CountryId] TINYINT NOT NULL,
  CONSTRAINT [FK_Country_City] FOREIGN KEY ([CountryId]) REFERENCES [Country]([CountryId])
)
GO

CREATE TABLE [dbo].[User]
(
  [UserId] INT PRIMARY KEY IDENTITY (1, 1),
  [Email] VARCHAR(254) NOT NULL UNIQUE,
  [Password] NVARCHAR(254) NOT NULL
)
GO

CREATE TABLE [dbo].[UserDetails]
(
  [UserId] INT NOT NULL,
  [FullName] VARCHAR(512),
  [CountryId] TINYINT,
  [CityId] INT,
  [DateOfBirth] DATE,
  CONSTRAINT [FK_User_UserDetails] FOREIGN KEY ([UserId]) REFERENCES [User]([UserId]),
  CONSTRAINT [FK_Country_UserDetails] FOREIGN KEY ([CountryId]) REFERENCES [Country]([CountryId]),
  CONSTRAINT [FK_City_UserDetails] FOREIGN KEY ([CityId]) REFERENCES [City]([CityId])
)
GO
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.UserStore.Models;

namespace MicroEcommerce.UserStore.Persistence
{
    public interface IUserRepository
    {
        Task<int> Add(User user);
        Task Update(User user);
        Task Delete(User user);
        Task Delete(int id);
        Task<User> GetUserById(int id);
        Task<User> GetUsersByEmail(string email);
        Task<IEnumerable<User>> GetAllUsers();
        Task<bool> SignIn(string email, string password);
    }
}
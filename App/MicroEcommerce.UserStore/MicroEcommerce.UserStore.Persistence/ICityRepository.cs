﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.UserStore.Models;

namespace MicroEcommerce.UserStore.Persistence
{
    public interface ICityRepository
    {
        int Add(City city);
        int Delete(City city);
        Task<IEnumerable<City>> GetCitiesByCountry(Country country);
        Task<City> GetCityByName(string name);
    }
}
﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using MicroEcommerce.UserStore.Models;

namespace MicroEcommerce.UserStore.Persistence
{
    public class CountryRepository: ICountryRepository
    {
        private readonly string _connectionString;

        public CountryRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        
        public int Add(Country country)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                country.CountryId = connection.QueryFirstOrDefault<int>(@"INSERT INTO [Country]([Name]) VALUES (@Name); SELECT CAST(SCOPE_IDENTITY() as int)", country);
            }
            return country.CountryId;
        }

        public int Delete(Country country)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                country.CountryId = connection.QueryFirstOrDefault<int>(@"DELETE FROM [Country] WHERE [CountryId] = @CountryId", country);
            }
            return country.CountryId;
        }

        public Task<IEnumerable<Country>> GetAllCountries()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.QueryAsync<Country>("SELECT * FROM [Country]");
            }
        }

        public Task<Country> GetCountryByName(string name)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.QueryFirstOrDefaultAsync<Country>("SELECT * FROM [Country] WHERE [Name] = @Name", new { Name = name });
            }
        }
    }
}
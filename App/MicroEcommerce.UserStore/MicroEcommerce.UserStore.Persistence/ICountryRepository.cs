﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.UserStore.Models;

namespace MicroEcommerce.UserStore.Persistence
{
    public interface ICountryRepository
    {
        int Add(Country country);
        int Delete(Country country);
        Task<IEnumerable<Country>> GetAllCountries();
        Task<Country> GetCountryByName(string name);
    }
}
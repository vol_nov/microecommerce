﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using MicroEcommerce.UserStore.Models;

namespace MicroEcommerce.UserStore.Persistence
{
    public class CityRepository: ICityRepository
    {
        private readonly string _connectionString;
        
        public CityRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        
        public int Add(City city)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                city.CityId = connection.QueryFirstOrDefault<int>(@"INSERT INTO [City]([Name], [CountryId]) VALUES (@Name, @CountryId); SELECT CAST(SCOPE_IDENTITY() as int)", city);
            }
            return city.CityId;
        }

        public int Delete(City city)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                city.CityId = connection.QueryFirstOrDefault<int>(@"DELETE FROM [City] WHERE [CityId] = @CityId", city);
            }
            return city.CityId;
        }

        public Task<IEnumerable<City>> GetCitiesByCountry(Country country)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.QueryAsync<City>("SELECT * FROM [City] WHERE [CountryId] = @CountryId", country);
            }
        }

        public Task<City> GetCityByName(string name)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.QueryFirstOrDefaultAsync<City>("SELECT * FROM [City] WHERE [Name] = @Name", new {Name = name});
            }
        }
    }
}
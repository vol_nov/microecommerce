﻿using System;

namespace MicroEcommerce.UserStore.Models
{
    public class UserDetails
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public Country Country { get; set; }
        public City City { get; set; }
        public DateTime DateOfBirth { get; set; }
        
        public UserDetails()
        {     
            Country = new Country();
            City = new City();
        }
        
        public UserDetails(int userId, string fullName)
        {
            UserId = userId;
            FullName = fullName;
        }

        public UserDetails(int userId, string fullName, Country country, City city)
        {
            UserId = userId;
            FullName = fullName;
            Country = country;
            City = city;
        }

        public UserDetails(int userId, string fullName, Country country, City city, DateTime dateOfBirth)
        {
            UserId = userId;
            FullName = fullName;
            Country = country;
            City = city;
            DateOfBirth = dateOfBirth;
        }
    }
}
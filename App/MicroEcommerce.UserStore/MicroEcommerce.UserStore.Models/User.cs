﻿using System;

namespace MicroEcommerce.UserStore.Models
{
    public class User
    {   
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserDetails UserDetails { get; set; }
        
        public User()
        {        
            UserDetails = new UserDetails();
        }

        public User(int id, string email, string password, UserDetails userDetails)
        {
            UserId = id;
            Email = email;
            Password = password;
            UserDetails = userDetails;
        }

        public User(int id, string email, string password)
        {
            UserId = id;
            Email = email;
            Password = password;
        }
    }
}
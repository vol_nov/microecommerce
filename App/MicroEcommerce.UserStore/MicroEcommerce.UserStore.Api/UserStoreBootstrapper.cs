﻿using MicroEcommerce.UserStore.Infrastructure;
using MicroEcommerce.SimpleInjector.Nancy;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace MicroEcommerce.UserStore
{
    public class UserStoreBootstrapper: SimpleInjectorNancyBootstrapper
    {
        public UserStoreBootstrapper(IConfigurationRoot configuration, ILogger logger): base(configuration, logger)
        {
            Packages = new[] {new Package()};
        }
    }
}
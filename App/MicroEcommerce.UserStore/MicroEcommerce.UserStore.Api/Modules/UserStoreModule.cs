﻿using System.Text;
using System.Threading.Tasks;
using MicroEcommerce.UserStore.BusinessLayer;
using MicroEcommerce.UserStore.Models;
using Nancy;
using Nancy.ModelBinding;
using Newtonsoft.Json;

namespace MicroEcommerce.UserStore.Modules
{
    public class UserStoreModule: NancyModule
    {
        private IUserService _userService;
        
        public UserStoreModule(IUserService userService) : base("/api/userstore")
        {
            _userService = userService;

            Get("/users/{id:int}", GetUserById);
            Get("/users", SignInUser);
            Post("/users", RegisterUser);
            Put("/users", UpdateUser);
            Delete("/users/{id:int}", DeleteUser);
        }

        public async Task<Response> GetUserById(dynamic args)
        {
            var user = await _userService.GetUser((int)args.id);
            return Response.AsJson(user).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<Response> SignInUser(dynamic args)
        {
            string email = this.Request.Query["email"];
            string password = this.Request.Query["password"];
            bool result = await _userService.SignIn(email, password);
            return Response.AsJson(result).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<Response> RegisterUser(dynamic args)
        {
            var user = ReadUser(this.Request);
            await _userService.Register(user);
            return Response.AsJson(user).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<Response> UpdateUser(dynamic args)
        {
            var user = this.Bind<User>();
            await _userService.Update(user);
            return Response.AsJson(user).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<Response> DeleteUser(dynamic args)
        {
            await _userService.Remove(args.id);
            return Response.AsJson((int)args.id).WithStatusCode(HttpStatusCode.OK);
        }

        private User ReadUser(Request request)
        {
            byte[] bytes = new byte[request.Body.Length];
            request.Body.Read(bytes, 0, (int)request.Body.Length);
            string jsonStr = Encoding.UTF8.GetString(bytes);
            return JsonConvert.DeserializeObject<User>(jsonStr);
        }
    }
}
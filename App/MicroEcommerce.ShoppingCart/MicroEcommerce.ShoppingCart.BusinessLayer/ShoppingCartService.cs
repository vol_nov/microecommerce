﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.PubSub;
using MicroEcommerce.ShoppingCart.Models;
using MicroEcommerce.ShoppingCart.Persistence;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Cart = MicroEcommerce.ShoppingCart.Models.ShoppingCart;

namespace MicroEcommerce.ShoppingCart.BusinessLayer
{
    public interface IShoppingCartService
    {
        Task<Cart> Add(Cart cart);
        Task Update(Cart cart);
        Task Delete(int cartId);
        Task<IEnumerable<Cart>> GetAllCarts();
        Task<Cart> GetCartById(int cartId);
        Task AddProductToCart(CartProduct cartProduct);
        Task DeleteProductFromCart(int cartId, Guid productId);
        Task SetCartBought(int cartId, bool bought);
    }
    
    public class ShoppingCartService: IShoppingCartService
    {
        private readonly IShoppingCartRepository _repository;
        private readonly ISendExchange _sendExchange;
        
        public ShoppingCartService(IShoppingCartRepository repository,
            ISendExchange sendExchange)
        {
            _repository = repository;
            _sendExchange = sendExchange;
        }
        
        public async Task<Cart> Add(Cart cart)
        {
            return await _repository.Add(cart);
        }

        public async Task Update(Cart cart)
        {
            await _repository.Update(cart);
        }

        public async Task Delete(int cartId)
        {
            await _repository.Delete(cartId);
        }

        public async Task<IEnumerable<Cart>> GetAllCarts()
        {
            return await _repository.GetAllCarts();
        }

        public async Task<Cart> GetCartById(int cartId)
        {
            return await _repository.GetCartById(cartId);
        }

        public async Task AddProductToCart(CartProduct cartProduct)
        {
            await _repository.AddProductToCart(cartProduct);
        }

        public async Task DeleteProductFromCart(int cartId, Guid productId)
        {
            await _repository.DeleteProductFromCart(cartId, productId);
        }

        public async Task SetCartBought(int cartId, bool bought)
        {
            var shoppingCart = await _repository.GetCartById(cartId);
            await _repository.SetCartBought(cartId, bought);
            foreach (var product in shoppingCart.Products)
            {
                _sendExchange.Send("productBought", JsonConvert.SerializeObject(
                    new { ProductId = product.ProductId, Bought = bought, UserId = shoppingCart.UserId, Price = product.Price }));
            }
        }
    }
}

CREATE TABLE [dbo].[ShoppingCart]
(
  [ShoppingCartId] INT PRIMARY KEY IDENTITY(1,1),
  [UserId] INT NOT NULL,
  [CreationDate] DATETIME NOT NULL,
  [Bought] BIT NOT NULL
)
GO

CREATE TABLE [dbo].[ShoppingCartProduct]
(
  [ShoppingCartId] INT NOT NULL,
  [ProductId] UNIQUEIDENTIFIER NOT NULL,
  [Price] DECIMAL NOT NULL,
  [Count] INT NOT NULL
)
GO
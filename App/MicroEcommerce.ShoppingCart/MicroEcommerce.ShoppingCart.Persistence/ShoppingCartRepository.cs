﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MicroEcommerce.ShoppingCart.Models;
using Cart = MicroEcommerce.ShoppingCart.Models.ShoppingCart;

namespace MicroEcommerce.ShoppingCart.Persistence
{
    public interface IShoppingCartRepository
    {
        Task<Cart> Add(Cart cart);
        Task Update(Cart cart);
        Task Delete(int cartId);
        Task<IEnumerable<Cart>> GetAllCarts();
        Task<Cart> GetCartById(int cartId);
        Task AddProductToCart(CartProduct product);
        Task DeleteProductFromCart(int cartId, Guid productId);
        Task SetCartBought(int cartId, bool bought);
    }

    public class ShoppingCartRepository : IShoppingCartRepository
    {
        private readonly string _connectionString;

        public ShoppingCartRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<Cart> Add(Cart cart)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var cartId = await connection.QueryFirstOrDefaultAsync<int>(@"INSERT INTO [dbo].[ShoppingCart]
                       ([UserId]
                       ,[CreationDate]
                       ,[Bought])
                VALUES
                       (@UserId
                       ,@CreationDate
                       ,@Bought);
                SELECT CAST(SCOPE_IDENTITY() as int)", cart);

                cart.ShoppingCartId = cartId;

                if (cart.Products != null && cart.Products.Any())
                {
                    var values = string.Join(",",
                        cart.Products.Select(product => $"({cart.ShoppingCartId}, \'{product.ProductId}\', {product.Price}, {product.Count})"));
                    await connection.ExecuteAsync(
                        $"INSERT INTO [dbo].[ShoppingCartProduct]([ShoppingCartId], [ProductId], [Price], [Count]) VALUES {values}");
                }

                return cart;
            }
        }

        public async Task Update(Cart cart)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"UPDATE [dbo].[ShoppingCart]
   SET [UserId] = @UserId
      ,[CreationDate] = @CreationDate
      ,[Bought] = @Bought
 WHERE ShoppingCartId = @ShoppingCartId", cart);
            }
        }

        public async Task Delete(int cartId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"DELETE FROM [dbo].[ShoppingCart] 
    WHERE [ShoppingCartId] = @ShoppingCartId; DELETE FROM [dbo].[ShoppingCartProduct] 
    WHERE [ShoppingCartId] = @ShoppingCartId;", new {ShoppingCartId = cartId});
            }
        }

        public async Task<IEnumerable<Cart>> GetAllCarts()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var cartDictionary = new ConcurrentDictionary<int, Cart>();
                await connection.QueryAsync<Cart, CartProduct, Cart>(@"SELECT cart.[ShoppingCartId]
                    ,cart.[UserId]
                    ,cart.[CreationDate]
                    ,cart.[Bought]
                    ,prod.[ShoppingCartId]
                    ,prod.[ProductId]
                    ,prod.[Price]
                    ,prod.[Count]
                FROM [dbo].[ShoppingCart] cart
                LEFT JOIN [dbo].[ShoppingCartProduct] prod ON cart.[ShoppingCartId] = prod.[ShoppingCartId]
                ", (cart, product) =>
                {
                    var cartEntry = cartDictionary.GetOrAdd(cart.ShoppingCartId, cart);
                    if (product != null)
                    {
                        if (product.ProductId != Guid.Empty)
                        {
                            if (cartEntry.Products == null)
                                cartEntry.Products = new List<CartProduct>();
                            cartEntry.Products.Add(product);
                        }
                    }
                    return cartEntry;
                }, splitOn: "ShoppingCartId");
                return cartDictionary.Values;
            }
        }

        public async Task<Cart> GetCartById(int cartId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var cartDictionary = new ConcurrentDictionary<int, Cart>();
                await connection.QueryAsync<Cart, CartProduct, Cart>(@"SELECT cart.[ShoppingCartId]
                    ,cart.[UserId]
                    ,cart.[CreationDate]
                    ,cart.[Bought]
                    ,cart.[ShoppingCartId]
                    ,product.[ProductId]
                    ,product.[Price]
                    ,product.[Count]
                FROM [dbo].[ShoppingCart] cart
                LEFT JOIN [dbo].[ShoppingCartProduct] product 
                ON cart.[ShoppingCartId] = product.[ShoppingCartId]
                WHERE cart.[ShoppingCartId] = @ShoppingCartId
                ", (cart, product) =>
                {
                    var cartEntry = cartDictionary.GetOrAdd(cart.ShoppingCartId, cart);
                    if (product != null)
                    {
                        if (product.ProductId != Guid.Empty)
                        {
                            if (cartEntry.Products == null)
                                cartEntry.Products = new List<CartProduct>();
                            cartEntry.Products.Add(product);
                        }
                    }
                    return cartEntry;
                }, new { ShoppingCartId = cartId }, splitOn: "ShoppingCartId");

                return cartDictionary.Values.FirstOrDefault();
            }
        }

        public async Task AddProductToCart(CartProduct product)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"
INSERT INTO [dbo].[ShoppingCartProduct]
           ([ShoppingCartId]
           ,[ProductId]
           ,[Price]
           ,[Count])
     VALUES
           (@ShoppingCartId,
            @ProductId,
            @Price,
            @Count)", product);
            }
        }

        public async Task DeleteProductFromCart(int cartId, Guid productId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"DELETE 
FROM [dbo].[ShoppingCartProduct]
WHERE ShoppingCartId = @ShoppingCartId AND ProductId = @ProductId",
                    new {ShoppingCartId = cartId, ProductId = productId});
            }
        }

        public async Task SetCartBought(int cartId, bool bought)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"UPDATE [dbo].[ShoppingCart]
SET Bought = @Bought
WHERE ShoppingCartId = @ShoppingCartId",
                    new {ShoppingCartId = cartId, Bought = bought});
            }
        }
    }
}
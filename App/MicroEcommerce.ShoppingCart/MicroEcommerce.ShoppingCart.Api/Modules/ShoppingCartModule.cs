﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.ShoppingCart.BusinessLayer;
using MicroEcommerce.ShoppingCart.Models;
using Nancy;
using Nancy.ModelBinding;
using Cart = MicroEcommerce.ShoppingCart.Models.ShoppingCart;
namespace MicroEcommerce.ShoppingCart.Modules
{
    public class ShoppingCartModule: NancyModule
    {
        private readonly IShoppingCartService _service;
        
        public ShoppingCartModule(IShoppingCartService service) : base("/api/shoppingcart")
        {
            _service = service;

            Get("/cart", GetCarts);
            Get("/cart/{id}", GetCartById);
            Post("/cart", AddCart);
            Put("/cart", UpdateCart);
            Delete("/cart/{id}", DeleteCart);
            Post("/cart/product", AddProductToCart);
            Delete("/cart/{cartId}/product/{productId}", DeleteProductFromCart);
            Put("/cart/{cartId}/bought/{bought}", SetCartBought);
        }

        private async Task<object> SetCartBought(dynamic arg)
        {
            int cartId = arg.cartId;
            bool bought = arg.bought;

            await _service.SetCartBought(cartId, bought);
            
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
        
        private async Task<object> DeleteProductFromCart(dynamic arg)
        {
            Guid productId = arg.productId;
            int cartId = arg.cartId;

            await _service.DeleteProductFromCart(cartId, productId);

            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
        
        private async Task<object> AddProductToCart(dynamic arg)
        {
            var cartProduct = this.Bind<CartProduct>();
            await _service.AddProductToCart(cartProduct);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        private async Task<object> DeleteCart(dynamic arg)
        {
            int cartId = arg.id;
            await _service.Delete(cartId);
            return Negotiate.WithModel(cartId).WithStatusCode(HttpStatusCode.OK);
        }

        private async Task<object> GetCartById(dynamic arg)
        {
            int cartId = arg.id;
            var cart = await _service.GetCartById(cartId);
            return Negotiate.WithModel(cart).WithStatusCode(HttpStatusCode.OK);
        }

        private async Task<object> UpdateCart(dynamic arg)
        {
            var cart = BindCart();
            await _service.Update(cart);
            return Negotiate.WithModel(cart).WithStatusCode(HttpStatusCode.OK);
        }

        private async Task<object> AddCart(dynamic arg)
        {
            var cart = BindCart();
            cart = await _service.Add(cart);
            return Negotiate.WithModel(cart).WithStatusCode(HttpStatusCode.OK);
        }

        private async Task<object> GetCarts(dynamic arg)
        {
            var carts = await _service.GetAllCarts();
            return Negotiate.WithModel(carts).WithStatusCode(HttpStatusCode.OK);
        }

        private Cart BindCart()
        {
            var cart = this.Bind<Cart>();
            if (cart.CreationDate is null)
            {
                cart.CreationDate = DateTime.Now;
            }
            return cart;
        }
    }
}
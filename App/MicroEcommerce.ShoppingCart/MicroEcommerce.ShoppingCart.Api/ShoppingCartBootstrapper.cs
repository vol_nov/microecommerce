﻿using MicroEcommerce.ShoppingCart.BusinessLayer;
using MicroEcommerce.ShoppingCart.Infrastructure;
using MicroEcommerce.SimpleInjector.Nancy;
using Microsoft.Extensions.Configuration;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Configuration;
using Nancy.TinyIoc;
using Serilog;

namespace MicroEcommerce.ShoppingCart
{
    public class ShoppingCartBootstrapper: SimpleInjectorNancyBootstrapper
    {
        public ShoppingCartBootstrapper(IConfigurationRoot configurationRoot, ILogger logger) : base(configurationRoot, logger)
        {
            Packages = new[] { new Package() };
        }

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            base.RequestStartup(container, pipelines, context);

            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
            {
                ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                            .WithHeader("Access-Control-Allow-Methods", "POST,GET,DELETE,PUT")
                            .WithHeader("Allow", "POST,GET,PUT,DELETE")
                            .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");
            });
        }
        
        public override void Configure(INancyEnvironment environment)
        {
            var config = new TraceConfiguration(enabled: true, displayErrorTraces: true);
            environment.AddValue(config);
        }
    }
}
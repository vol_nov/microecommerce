﻿using System;

namespace MicroEcommerce.ShoppingCart.Models
{
    [Serializable]
    public class CartProduct
    {
        public int ShoppingCartId { get; set; }
        public Guid ProductId { get; set; }
        public decimal Price { get; set; }
        public int Count { get; set; }
        
        public CartProduct()
        {
            
        }
    }
}
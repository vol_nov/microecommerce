﻿using System;
using System.Collections.Generic;

namespace MicroEcommerce.ShoppingCart.Models
{
    [Serializable]
    public class ShoppingCart
    {
        public int ShoppingCartId { get; set; }
        public int UserId { get; set; }
        public bool Bought { get; set; }
        public DateTime? CreationDate { get; set; }
        public ICollection<CartProduct> Products { get; set; }
        
        public ShoppingCart()
        {
            Products = new List<CartProduct>();
        }
    }
}

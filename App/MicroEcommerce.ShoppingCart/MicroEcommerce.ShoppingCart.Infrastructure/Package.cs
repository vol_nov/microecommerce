﻿using System;
using MicroEcommerce.PubSub;
using MicroEcommerce.ShoppingCart.BusinessLayer;
using MicroEcommerce.ShoppingCart.Persistence;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace MicroEcommerce.ShoppingCart.Infrastructure
{
    public class Package: IPackage
    {
        public void RegisterServices(Container container)
        {
            RegisterPersistenceDependencies(container);
            RegisterBusinessLayerDependencies(container);
        }

        private void RegisterBusinessLayerDependencies(Container container)
        {
            container.Register<IShoppingCartService, ShoppingCartService>(Lifestyle.Scoped);
        }

        private void RegisterPersistenceDependencies(Container container)
        {
            container.Register<IShoppingCartRepository>(() => new ShoppingCartRepository(GetConnectionString(container)), Lifestyle.Scoped);
            container.Register<ISendExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new SendExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);
        }
        
        private string GetConnectionString(Container container)
        {
            return container.GetInstance<IConfigurationRoot>().GetConnectionString("DefaultConnectionString");
        }
    }
}
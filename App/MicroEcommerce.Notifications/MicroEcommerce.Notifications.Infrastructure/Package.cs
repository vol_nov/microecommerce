﻿using System;
using MicroEcommerce.Notifications.BusinessLayer;
using MicroEcommerce.Notifications.Persistence;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace MicroEcommerce.Notifications.Infrastructure
{
    public class Package : IPackage
    {
        public void RegisterServices(Container container)
        {
            RegisterBusinessLayerDependencies(container);
            RegisterPersistenceDependencies(container);
        }

        public void RegisterBusinessLayerDependencies(Container container)
        {
            container.Register<INotificationService, NotificationService>(Lifestyle.Scoped);
            container.Register<IReceiveExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new ReceiveExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);  
        }

        public void RegisterPersistenceDependencies(Container container)
        {
            container.Register<INotificationRepository>(
                () => new NotificationRepository(GetConnectionString(container)), Lifestyle.Scoped);
        }
        
        private string GetConnectionString(Container container)
        {
            return container.GetInstance<IConfigurationRoot>().GetConnectionString("DefaultConnectionString");
        }
    }
}
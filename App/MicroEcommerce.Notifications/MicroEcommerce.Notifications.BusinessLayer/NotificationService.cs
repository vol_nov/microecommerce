﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.Notifications.Models;
using MicroEcommerce.Notifications.Persistence;

namespace MicroEcommerce.Notifications.BusinessLayer
{
    public interface INotificationService
    {
        Task<Notification> Add(Notification notification);
        Task Update(Notification notification);
        Task Delete(long notificationId);
        Task<IEnumerable<Notification>> GetAllNotifications();
        Task<Notification> GetNotificationById(long notificationId);
        Task<IEnumerable<Notification>> GetNotificationByUserId(long notificationId, bool notSeenOnly = true);
        Task SetNotificationAsSeen(long notificationId);
    }

    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _notificationRepository;

        public NotificationService(INotificationRepository notificationRepository)
        {
            _notificationRepository = notificationRepository;
        }

        public async Task<Notification> Add(Notification notification)
        {
            return await _notificationRepository.Add(notification);
        }

        public async Task Update(Notification notification)
        {
            await _notificationRepository.Update(notification);
        }

        public async Task Delete(long notificationId)
        {
            await _notificationRepository.Delete(notificationId);
        }

        public async Task<IEnumerable<Notification>> GetAllNotifications()
        {
            return await _notificationRepository.GetAllNotifications();
        }

        public async Task<Notification> GetNotificationById(long notificationId)
        {
            return await _notificationRepository.GetNotificationById(notificationId);
        }

        public async Task<IEnumerable<Notification>> GetNotificationByUserId(long notificationId, bool notSeenOnly = true)
        {
            return await _notificationRepository.GetNotificationByUserId(notificationId, notSeenOnly);
        }

        public async Task SetNotificationAsSeen(long notificationId)
        {
            await _notificationRepository.SetNotificationAsSeen(notificationId);
        }
    }
}
﻿using System;

namespace MicroEcommerce.Notifications.Models
{
    [Serializable]
    public enum NotificationType
    {
        Error, Hand, Information, Warning, Exclamation, Question
    }
    
    [Serializable]
    public class Notification
    {
        public long NotificationId { get; set; }
        public string Text { get; set; }
        public string Link { get; set; }
        public string ImagePath { get; set; }
        public NotificationType Type { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Seen { get; set; }
        public int UserId { get; set; }
    }
}
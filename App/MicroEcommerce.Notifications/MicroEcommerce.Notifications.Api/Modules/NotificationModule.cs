﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.Notifications.BusinessLayer;
using MicroEcommerce.Notifications.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Notifications.Modules
{
    public class NotificationModule : NancyModule
    {
        private readonly INotificationService _notificationService;
        
        public NotificationModule(INotificationService notificationService) : base("/api/notify")
        {
            _notificationService = notificationService;
            
            Get("/notification", GetAllNotifications);
            Get("/notification/{id}", GetNotificationById);
            Get("/notification/user/{userId}/notseen/{notSeenOnly:bool}", GetNotificationByUserId);
            Post("/notification", AddNotification);
            Put("/notification", UpdateNotification);
            Put("/notification/{id}", SetNotificationAsSeen);
            Delete("/notification/{id}", DeleteNotification);
        }
        
        public async Task<object> AddNotification(dynamic args)
        {
            var notification = this.Bind<Notification>();
            notification = await _notificationService.Add(notification);
            return Negotiate.WithModel(notification).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateNotification(dynamic args)
        {
            var notification = this.Bind<Notification>();
            await _notificationService.Update(notification);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteNotification(dynamic args)
        {
            long notificationId = args.id;
            await _notificationService.Delete(notificationId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetAllNotifications(dynamic args)
        {
            var notifications = await _notificationService.GetAllNotifications();
            return Negotiate.WithModel(notifications).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetNotificationById(dynamic args)
        {
            long notificationId = args.id;
            var notifications = await _notificationService.GetNotificationById(notificationId);
            return Negotiate.WithModel(notifications).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetNotificationByUserId(dynamic args)
        {
            int userId = args.userId;
            bool notSeenOnly = args.notSeenOnly;
            var notifications = await _notificationService.GetNotificationByUserId(userId, notSeenOnly);
            return Negotiate.WithModel(notifications).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> SetNotificationAsSeen(dynamic args)
        {
            long notificationId = args.id;
            await _notificationService.SetNotificationAsSeen(notificationId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
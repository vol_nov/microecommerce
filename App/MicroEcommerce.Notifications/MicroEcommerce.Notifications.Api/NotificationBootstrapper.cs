﻿using MicroEcommerce.Notifications.Infrastructure;
using MicroEcommerce.SimpleInjector.Nancy;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace MicroEcommerce.Notifications
{
    public class NotificationBootstrapper : SimpleInjectorNancyBootstrapper
    {
        public NotificationBootstrapper(IConfigurationRoot configurationRoot, ILogger logger) : base(configurationRoot, logger)
        {
            Packages = new[] {new Package()};
        }
    }
}
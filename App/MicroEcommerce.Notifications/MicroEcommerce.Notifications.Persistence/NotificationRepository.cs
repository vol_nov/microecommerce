﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using MicroEcommerce.Notifications.Models;

namespace MicroEcommerce.Notifications.Persistence
{
    public interface INotificationRepository
    {
        Task<Notification> Add(Notification notification);
        Task Update(Notification notification);
        Task Delete(long notificationId);
        Task<IEnumerable<Notification>> GetAllNotifications();
        Task<Notification> GetNotificationById(long notificationId);
        Task<IEnumerable<Notification>> GetNotificationByUserId(long notificationId, bool notSeenOnly = true);
        Task SetNotificationAsSeen(long notificationId);
    }

    public class NotificationRepository : INotificationRepository
    {
        private readonly string _connectionString;

        public NotificationRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<Notification> Add(Notification notification)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var id = await connection.QueryFirstOrDefaultAsync<long>(@"INSERT INTO [dbo].[Notifications]
                                                            ([Text]
                                                            ,[Link]
                                                            ,[ImagePath]
                                                            ,[Type]
                                                            ,[CreationDate]
                                                            ,[Seen]
                                                            ,[UserId])
                                                        VALUES
                                                            (@Text
                                                            ,@Link
                                                            ,@ImagePath
                                                            ,@Type
                                                            ,@CreationDate
                                                            ,@Seen
                                                            ,@UserId);
                                                        SELECT CAST(SCOPE_IDENTITY() as bigint)", notification);
                notification.NotificationId = id;

                return notification;
            }
        }

        public async Task Update(Notification notification)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"UPDATE [dbo].[Notifications]
                 SET [Text] = @Text
                    ,[Link] = @Link
                    ,[ImagePath] = @ImagePath
                    ,[Type] = @Type
                    ,[CreationDate] = @CreationDate
                    ,[Seen] = @Seen
                    ,[UserId] = @UserId
                WHERE [NotificationId] = @NotificationId", notification);
            }
        }

        public async Task Delete(long notificationId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(
                    @"DELETE FROM [dbo].[Notifications] WHERE [NotificationId] = @NotificationId",
                    new {NotificationId = notificationId});
            }
        }

        public async Task<IEnumerable<Notification>> GetAllNotifications()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return await connection.QueryAsync<Notification>(@"SELECT [NotificationId]
                        ,[Text]
                    ,[Link]
                    ,[ImagePath]
                    ,[Type]
                    ,[CreationDate]
                    ,[Seen]
                    ,[UserId]
                FROM [dbo].[Notifications]");
            }
        }

        public async Task<Notification> GetNotificationById(long notificationId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return await connection.QueryFirstOrDefaultAsync<Notification>(@"SELECT [NotificationId]
                        ,[Text]
                    ,[Link]
                    ,[ImagePath]
                    ,[Type]
                    ,[CreationDate]
                    ,[Seen]
                    ,[UserId]
                FROM [dbo].[Notifications]
                WHERE [NotificationId] = @NotificationId", new {NotificationId = notificationId});
            }
        }

        public async Task<IEnumerable<Notification>> GetNotificationByUserId(long notificationId, bool notSeenOnly = true)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var notSeenStr = notSeenOnly ? "AND [Seen] = 0" : "";
                return await connection.QueryAsync<Notification>($@"SELECT [NotificationId]
                        ,[Text]
                    ,[Link]
                    ,[ImagePath]
                    ,[Type]
                    ,[CreationDate]
                    ,[Seen]
                    ,[UserId]
                FROM [dbo].[Notifications]
                WHERE [UserId] = @NotificationId {notSeenStr}", new {NotificationId = notificationId});
            }
        }

        public async Task SetNotificationAsSeen(long notificationId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.ExecuteAsync(@"UPDATE [dbo].[Notifications]
                 SET [Seen] = @Seen
                WHERE [NotificationId] = @NotificationId", new {NotificationId = notificationId, Seen = true});
            }
        }
    }
}
CREATE TABLE [dbo].[Notifications]
(
  [NotificationId] BIGINT PRIMARY KEY IDENTITY (1, 1),
  [Text] NVARCHAR(MAX) NOT NULL,
  [Link] VARCHAR(MAX),
  [ImagePath] VARCHAR(MAX),
  [Type] TINYINT NOT NULL,
  [CreationDate] DATETIME2 NOT NULL,
  [Seen] BIT NOT NULL,
  [UserId] INT NOT NULL
)
GO
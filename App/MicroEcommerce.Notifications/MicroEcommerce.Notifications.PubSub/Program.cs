﻿using System;
using MicroEcommerce.Logging;
using MicroEcommerce.Notifications.BusinessLayer;
using MicroEcommerce.Notifications.Infrastructure;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroEcommerce.Notifications.PubSub
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Notifications PubSub";
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            
            var configuration = builder.Build();
            var logger = LoggerConfig.Create();

            var container = new Container();
            var package = new Package();
            
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            container.Register(() => configuration);
            container.Register(() => logger);

            using (AsyncScopedLifestyle.BeginScope(container))
            {
                package.RegisterServices(container);

                var notificationsPubSub = new NotificationsSubscriber( 
                    container.GetInstance<IReceiveExchange>(),
                    container.GetInstance<INotificationService>(),
                    logger);

                notificationsPubSub.RegisterExchanges();

                Console.WriteLine("Notifications PubSub is Up");

                Console.ReadLine();
            }
        }
    }
}
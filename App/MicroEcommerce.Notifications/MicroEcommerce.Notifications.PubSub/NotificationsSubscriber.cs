﻿using System;
using System.Text;
using System.Threading.Tasks;
using MicroEcommerce.Notifications.BusinessLayer;
using MicroEcommerce.Notifications.Models;
using MicroEcommerce.PubSub;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using Serilog;

namespace MicroEcommerce.Notifications.PubSub
{
    public class NotificationsSubscriber : ISubscriber
    {
        private readonly IReceiveExchange _receiveExchange;
        private readonly INotificationService _noficationService;
        private readonly ILogger _logger;

        public NotificationsSubscriber(IReceiveExchange receiveExchange, 
            INotificationService noficationService,
            ILogger logger)
        {
            _receiveExchange = receiveExchange;
            _noficationService = noficationService;
            _logger = logger;
        }

        public async Task OnProductAdded(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);
            dynamic product = JsonConvert.DeserializeObject(jsonProduct);
            
            await _noficationService.Add(new Notification()
            {
                CreationDate = DateTime.Now,
                Seen = false,
                Text = "Product added!",
                Type = NotificationType.Information,
                UserId = (int)product.UserId
            });

            _logger.Information($"Product added: {jsonProduct}");
        }

        public async Task OnProductCommented(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);

            dynamic product = JsonConvert.DeserializeObject(jsonProduct);
            
            await _noficationService.Add(new Notification()
            {
                CreationDate = DateTime.Now,
                Seen = false,
                Text = "Product commented!",
                Type = NotificationType.Information,
                UserId = (int)product.UserId
            });

            _logger.Information($"Product commented: {jsonProduct}");
        }

        public async Task OnProductLiked(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);

            dynamic product = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(body));

            _logger.Information($"Product liked: {jsonProduct}");

            await _noficationService.Add(new Notification()
            {
                CreationDate = DateTime.Now,
                Seen = false,
                Text = "Product liked!",
                Type = NotificationType.Information,
                UserId = (int)product.UserId
            });
        }
        
        public async Task OnProductBought(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);

            dynamic product = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(body));

            _logger.Information($"Product bought: {jsonProduct}");

            await _noficationService.Add(new Notification()
            {
                CreationDate = DateTime.Now,
                Seen = false,
                Text = "Product bought!",
                Type = NotificationType.Information,
                UserId = (int)product.UserId
            });
        }

        public void RegisterExchanges()
        {
            _receiveExchange.Receive("productAdded", (sender, eventArgs) =>
            {
                OnProductAdded(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productCommented", (sender, eventArgs) =>
            {
                OnProductCommented(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productLiked", (sender, eventArgs) =>
            {
                OnProductLiked(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productBought", (sender, eventArgs) =>
            {
                OnProductBought(sender, eventArgs).Wait();
            });
        }
    }
}
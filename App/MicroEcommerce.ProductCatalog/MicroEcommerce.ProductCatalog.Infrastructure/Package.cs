﻿using System;
using MicroEcommerce.ProductCatalog.BusinessLayer;
using MicroEcommerce.ProductCatalog.Persistence;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace MicroEcommerce.ProductCatalog.Infrastructure
{
    public class Package: IPackage
    {
        public void RegisterServices(Container container)
        {
            RegisterBusinessLayerDependencies(container);
            RegisterPersistenceDependencies(container);
        }

        private void RegisterBusinessLayerDependencies(Container container)
        {
            container.Register<IProductRepository>(() => new ProductRepository(GetConnectionString(container)), Lifestyle.Scoped);
            container.Register<IProductCommentRepository>(() => new ProductCommentRepository(GetConnectionString(container)), Lifestyle.Scoped);
            container.Register<IProductAttachmentRepository>(() => new ProductAttachmentRepository(GetConnectionString(container)), Lifestyle.Scoped);
            container.Register<IProductCategoryRepository>(() => new ProductCategoryRepository(GetConnectionString(container)), Lifestyle.Scoped);
        }
        
        private void RegisterPersistenceDependencies(Container container)
        {
            container.Register<IAttachmentService, AttachmentService>(Lifestyle.Scoped);
            container.Register<ICommentService, CommentService>(Lifestyle.Scoped);
            container.Register<IProductService, ProductService>(Lifestyle.Scoped);
            container.Register<ICategoryService, CategoryService>(Lifestyle.Scoped);
            container.Register<ISendExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new SendExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);

            container.Register<IReceiveExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new ReceiveExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);
        }

        private string GetConnectionString(Container container)
        {
            return container.GetInstance<IConfigurationRoot>().GetConnectionString("DefaultConnectionString");
        }
    }
}
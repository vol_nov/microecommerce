﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace MicroEcommerce.ProductCatalog.Persistence
{
    public interface IProductCommentRepository: IRepository<ProductComment>
    {
        Task<ICollection<ProductComment>> GetProductComments(Guid productId);
    }
    
    public class ProductCommentRepository: RepositoryBase<ProductComment>, IProductCommentRepository
    {
        private const string _tableName = "comments";
        
        public ProductCommentRepository(string connectionString) : base(connectionString, _tableName)
        {
        }

        public async Task<ICollection<ProductComment>> GetProductComments(Guid productId)
        {
            var filterByProductId = Builders<BsonDocument>.Filter.Eq("ProductId", productId);
            var comments = await Collection.Find(filterByProductId).ToListAsync();
            return comments.Select(c => BsonSerializer.Deserialize<ProductComment>(c)).ToList();
        }

        protected override void SetIdentity(ProductComment item, BsonDocument bson)
        {
            item.CommentId = bson["_id"].AsGuid;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MicroEcommerce.ProductCatalog.Persistence
{
    public interface IRepository<T> where T: class
    {
        IMongoClient MongoClient { get; set; }
        IMongoDatabase MongoDatabase { get; set; }
        IMongoCollection<BsonDocument> Collection { get; set; }
        
        Task Add(T item);
        Task Update(T item);
        Task Delete(T item);
        Task Delete(Guid id);
        Task<ICollection<T>> GetAll();
        Task<T> GetItemById(Guid id);
    }
}
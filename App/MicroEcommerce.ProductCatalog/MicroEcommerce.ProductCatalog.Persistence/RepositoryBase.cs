﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace MicroEcommerce.ProductCatalog.Persistence
{
    public abstract class RepositoryBase<T>: IRepository<T> where T: class, new()
    {
        public IMongoClient MongoClient { get; set; }
        public IMongoDatabase MongoDatabase { get; set; }
        public IMongoCollection<BsonDocument> Collection { get; set; }

        protected RepositoryBase(string connectionString, string collectionName)
        {
            var mongoUrl = new MongoUrlBuilder(connectionString).ToMongoUrl();
            MongoClient = new MongoClient(mongoUrl);
            MongoDatabase = MongoClient.GetDatabase(mongoUrl.DatabaseName);
            Collection = MongoDatabase.GetCollection<BsonDocument>(collectionName);
        }
        
        public async Task Add(T item)
        {
            var bson = item.ToBsonDocument();
            await Collection.InsertOneAsync(bson);
            SetIdentity(item, bson);
        }

        protected abstract void SetIdentity(T item, BsonDocument bson);
        
        public async Task Update(T item)
        {
            var bson = item.ToBsonDocument();
            var filterById = Builders<BsonDocument>.Filter.Eq("_id", bson["_id"]);
            await Collection.FindOneAndReplaceAsync(filterById, bson);
        }
        
        public async Task Delete(T item)
        {
            var filterById = Builders<BsonDocument>.Filter.Eq("_id", item.ToBsonDocument()["_id"]);
            await Collection.FindOneAndDeleteAsync(filterById);
        }

        public async Task Delete(Guid id)
        {
            var filterById = Builders<BsonDocument>.Filter.Eq("_id", id);
            await Collection.FindOneAndDeleteAsync(filterById);
        }

        public async Task<ICollection<T>> GetAll()
        {
            var results = await Collection.Find(_ => true).ToListAsync();
            return results.Select(r => BsonSerializer.Deserialize<T>(r)).ToList();
        }

        public async Task<T> GetItemById(Guid id)
        {
            var filterById = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = await Collection.Find(filterById).FirstOrDefaultAsync();
            return BsonSerializer.Deserialize<T>(result);
        }
    }
}
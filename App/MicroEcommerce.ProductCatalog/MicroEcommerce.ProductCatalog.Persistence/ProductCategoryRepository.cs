﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.Models;
using MongoDB.Bson;

namespace MicroEcommerce.ProductCatalog.Persistence
{
    public interface IProductCategoryRepository: IRepository<ProductCategory>
    {
        
    }
    
    public class ProductCategoryRepository: RepositoryBase<ProductCategory>, IProductCategoryRepository
    {
        private const string _tableName = "categories";

        public ProductCategoryRepository(string connectionString) : base(connectionString, _tableName)
        {

        }

        protected override void SetIdentity(ProductCategory item, BsonDocument bson)
        {
            item.CategoryId = bson["_id"].AsGuid;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace MicroEcommerce.ProductCatalog.Persistence
{
    public interface IProductAttachmentRepository: IRepository<ProductAttachment>
    {
        Task<ICollection<ProductAttachment>> GetProductAttachments(Guid productId);
    }
    
    public class ProductAttachmentRepository: RepositoryBase<ProductAttachment>, IProductAttachmentRepository
    {
        private const string _tableName = "attachments";
        
        public ProductAttachmentRepository(string connectionString) : base(connectionString, _tableName)
        {
        }

        public async Task<ICollection<ProductAttachment>> GetProductAttachments(Guid productId)
        {
            var filterByProductId = Builders<BsonDocument>.Filter.Eq("ProductId", productId);
            var attachments = await Collection.Find(filterByProductId).ToListAsync();
            return attachments.Select(a => BsonSerializer.Deserialize<ProductAttachment>(a)).ToList();
        }

        protected override void SetIdentity(ProductAttachment item, BsonDocument bson)
        {
            item.AttachmentId = bson["_id"].AsGuid;
        }
    }
}
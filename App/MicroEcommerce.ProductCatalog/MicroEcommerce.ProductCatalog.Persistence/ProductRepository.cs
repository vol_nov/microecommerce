﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace MicroEcommerce.ProductCatalog.Persistence
{
    public interface IProductRepository: IRepository<Product>
    {
        Task<ICollection<Product>> GetProductsByPrice(decimal min, decimal max);
        Task<ICollection<Product>> GetProductsByCategoryId(Guid categoryId);
        Task<ICollection<Product>> SearchProducts(string filter);
        Task<bool> Like(Guid productId, int userId);
    }
    
    public class ProductRepository: RepositoryBase<Product>, IProductRepository
    { 
        private const string _tableName = "products";
        private readonly IMongoCollection<Product> _productCollection;
        
        public ProductRepository(string connectionString) : base(connectionString, _tableName)
        {
            _productCollection = MongoDatabase.GetCollection<Product>("products");
        }

        public async Task<ICollection<Product>> GetProductsByPrice(decimal min, decimal max)
        {
            return await _productCollection.Find(prod => prod.Price > min && prod.Price < max).ToListAsync();
        }

        public async Task<ICollection<Product>> GetProductsByCategoryId(Guid categoryId)
        {
            return await _productCollection.Find(prod => 
                prod.Categories != null && prod.Categories.Any(c => c.CategoryId == categoryId)).ToListAsync();
        }

        public async Task<ICollection<Product>> SearchProducts(string filter)
        {
            return await _productCollection.Find(product => !string.IsNullOrEmpty(product.Header) && product.Header.ToLower().Contains(filter) ||
                                                            !string.IsNullOrEmpty(product.Description) && product.Description.ToLower().Contains(filter) ||
                                                            !string.IsNullOrEmpty(product.Manufacturer) && product.Manufacturer.ToLower().Contains(filter)).ToListAsync();
        }
        
        public async Task<bool> Like(Guid productId, int userId)
        {
            var product = await GetItemById(productId);

            if (product.Likes == null)
            {
                product.Likes = new List<int>();
            }
            
            bool contains = product.Likes.Contains(userId);

            if (contains)
            {
                product.Likes.Remove(userId);
            }
            else
            {
                product.Likes.Add(userId);
            }
            
            var filterById = Builders<BsonDocument>.Filter.Eq("_id", productId);
            var updateDefinition = Builders<BsonDocument>.Update.Set(doc => doc["Likes"], new BsonArray(product.Likes));
            await Collection.UpdateOneAsync(filterById, updateDefinition);
            
            return !contains;
        }

        protected override void SetIdentity(Product item, BsonDocument bson)
        {
            item.ProductId = bson["_id"].AsGuid;
        }
    }
}
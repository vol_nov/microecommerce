﻿using Microsoft.Extensions.Configuration;
using MicroEcommerce.ProductCatalog.Infrastructure;
using MicroEcommerce.SimpleInjector.Nancy;
using Serilog;

namespace MicroEcommerce.ProductCatalog
{
    public class ProductCatalogBootstrapper: SimpleInjectorNancyBootstrapper
    {
        public ProductCatalogBootstrapper(IConfigurationRoot configuration, ILogger logger) : base(configuration, logger)
        {
            Packages = new[] { new Package() };
        }
    }
}
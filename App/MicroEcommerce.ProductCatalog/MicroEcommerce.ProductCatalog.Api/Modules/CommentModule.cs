﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.BusinessLayer;
using MicroEcommerce.ProductCatalog.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.ProductCatalog.Modules
{
    public class CommentModule: NancyModule
    {
        private ICommentService _commentService;

        public CommentModule(ICommentService commentService) : base("/api/catalog")
        {
            _commentService = commentService;
            
            Get("/comments", GetComments);
            Get("/comments/{id}", GetCommentById);
            Get("/comments/product/{id}", GetProductComments);
            Post("/comments", AddComment);
            Put("/comments", UpdateComment);
            Delete("/comments/{id}", DeleteComment);
        }

        public async Task<object> GetComments(dynamic args)
        {
            var comments = await _commentService.GetItems();
            return Negotiate.WithModel(comments).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetCommentById(dynamic args)
        {
            var comment = await _commentService.GetItemById((Guid) args.id);
            return Negotiate.WithModel(comment).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetProductComments(dynamic args)
        {
            var comments = await _commentService.GetProductComments((Guid) args.id);
            return Negotiate.WithModel(comments).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddComment(dynamic args)
        {
            var comment = this.Bind<ProductComment>();
            await _commentService.Add(comment);
            return Negotiate.WithModel(comment).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> UpdateComment(dynamic args)
        {
            var comment = this.Bind<ProductComment>();
            await _commentService.Update(comment);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteComment(dynamic args)
        {
            Guid id = args.id;
            await _commentService.Remove(id);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
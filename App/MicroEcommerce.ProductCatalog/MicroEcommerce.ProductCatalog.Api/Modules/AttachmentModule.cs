﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.BusinessLayer;
using MicroEcommerce.ProductCatalog.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.ProductCatalog.Modules
{
    public class AttachmentModule: NancyModule
    {
        private IAttachmentService _attachmentService;

        public AttachmentModule(IAttachmentService attachmentService) : base("/api/catalog")
        {
            _attachmentService = attachmentService;
            
            Get("/attachments", GetAttachments);
            Get("/attachments/{id}", GetAttachmentById);
            Get("/attachments/product/{id}", GetProductAttachments);
            Post("/attachments", AddAttachment);
            Put("/attachments", UpdateAttachment);
            Delete("/attachments/{id}", DeleteAttachment);
        }
        
        public async Task<object> GetAttachments(dynamic args)
        {
            var attachments = await _attachmentService.GetItems();
            return Negotiate.WithModel(attachments).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetAttachmentById(dynamic args)
        {
            var attachment = await _attachmentService.GetItemById((Guid) args.id);
            return Negotiate.WithModel(attachment).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetProductAttachments(dynamic args)
        {
            var attachments = await _attachmentService.GetProductAttachments((Guid) args.id);
            return Negotiate.WithModel(attachments).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddAttachment(dynamic args)
        {
            var attachments = await GetAttachmentsFromHttpFiles(this.Request);
            foreach (var attachment in attachments)
            {
                await _attachmentService.Add(attachment);
            }
            return Negotiate.WithModel(attachments).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> UpdateAttachment(dynamic args)
        {
            var attachment = this.Bind<ProductAttachment>();
            await _attachmentService.Update(attachment);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteAttachment(dynamic args)
        {
            Guid id = args.id;
            await _attachmentService.Remove(id);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<IEnumerable<ProductAttachment>> GetAttachmentsFromHttpFiles(Request request)
        {
            var productAttachment = this.Bind<ProductAttachment>();

            var attachments = new List<ProductAttachment>();
            
            foreach (var httpFile in request.Files)
            {
                var bytes = new byte[httpFile.Value.Length];
                
                await httpFile.Value.ReadAsync(bytes, 0, bytes.Length);
                
                var fileName = Path.GetRandomFileName() + httpFile.Name.Substring(httpFile.Name.IndexOf('.'));
                var path = Directory.GetCurrentDirectory() + "/Files/" + fileName;
                
                using (var fs = new FileStream(path, FileMode.Create))
                {
                    await fs.WriteAsync(bytes, 0, bytes.Length);
                }
                
                attachments.Add(new ProductAttachment()
                {
                    Name = fileName, 
                    ProductId = productAttachment.ProductId,
                    Path = "/Files/" + fileName
                });
            }
            
            return attachments;
        }
    }
}
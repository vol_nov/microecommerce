﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.BusinessLayer;
using MicroEcommerce.ProductCatalog.Models;
using MicroEcommerce.PubSub;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.ProductCatalog.Modules
{
    public class ProductModule: NancyModule
    {
        private readonly IProductService _productService;
        private readonly ICommentService _commentService;
        private readonly IAttachmentService _attachmentService;
        
        public ProductModule(IProductService productService): base("/api/catalog")
        {
            _productService = productService; 
            
            Get("/products", GetProducts);
            Get("/products/search", SearchProduct);
            Get("/products/{id}", GetProductById);
            Post("/products", AddProduct);
            Put("/products", UpdateProduct);
            Delete("/products/{id}", DeleteProduct);
            Post("/products/{productId}/like/{userId}", LikeProduct);
        }

        public async Task<object> GetProducts(dynamic parameters)
        {
            var products = await _productService.GetItems();
            return Negotiate.WithModel(products).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetProductById(dynamic parameters)
        {
            var product = await _productService.GetItemById((Guid)parameters.id);
            return Negotiate.WithModel(product).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> SearchProduct(dynamic parameters)
        {
            IDictionary<string, object> queryDictionary = this.Request.Query;
            ICollection<Product> result;
            if (queryDictionary.ContainsKey("categoryId"))
            {
                result = await _productService.GetProductsByCategoryId((Guid)this.Request.Query.categoryId);
            }
            else if (queryDictionary.ContainsKey("minPrice") && queryDictionary.ContainsKey("maxPrice"))
            {
                result = await _productService.GetProductsByPrice((decimal)this.Request.Query.minPrice, (decimal)this.Request.Query.maxPrice);
            }
            else if (queryDictionary.ContainsKey("find"))
            {
                result = await _productService.SearchProducts((string)this.Request.Query.find);
            }
            else
            {
                return Negotiate.WithStatusCode(HttpStatusCode.BadRequest);
            }
            
            return Negotiate.WithModel(result).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> AddProduct(dynamic parameters)
        {
            var product = this.Bind<Product>();
            await _productService.Add(product);
            return Negotiate.WithModel(product).WithStatusCode(HttpStatusCode.Created);
        }
        
        public async Task<object> UpdateProduct(dynamic parameters)
        {
            var product = this.Bind<Product>();
            await _productService.Update(product);
            return Negotiate.WithModel(product).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> DeleteProduct(dynamic parameters)
        {
            await _productService.Remove((Guid)parameters.id);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> LikeProduct(dynamic parameters)
        {
            Guid productId = parameters.productId;
            int userId = parameters.userId;

            var result = await _productService.Like(productId, userId);
            
            return Negotiate.WithModel(result).WithStatusCode(HttpStatusCode.OK);
        }
    }
}
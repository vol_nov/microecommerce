﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using MicroEcommerce.ProductCatalog.BusinessLayer;
using MicroEcommerce.ProductCatalog.Models;
using Nancy;
using Nancy.Extensions;
using Nancy.IO;
using Nancy.ModelBinding;

namespace MicroEcommerce.ProductCatalog.Modules
{
    public class CategoryModule: NancyModule
    {
        private ICategoryService _categoryService;
        
        public CategoryModule(ICategoryService categoryService): base("/api/catalog")
        {
            _categoryService = categoryService;
            
            Get("/categories", GetCategories);
            Get("/categories/{id}", GetCategoryById);
            Post("/categories", AddCategory);
            Put("/categories", UpdateCategory);
            Delete("/categories/{id}", DeleteCategory);
        }

        public async Task<object> GetCategories(dynamic parameters)
        {
            var categories = await _categoryService.GetItems();
            return Negotiate.WithModel(categories).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetCategoryById(dynamic parameters)
        {
            Guid categoryId = parameters.id;
            var category = await _categoryService.GetItemById(categoryId);
            return Negotiate.WithModel(category).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> AddCategory(dynamic parameters)
        {
            var category = this.Bind<ProductCategory>();
            await _categoryService.Add(category);
            return Negotiate.WithModel(category).WithStatusCode(HttpStatusCode.Created);
        }
        
        public async Task<object> UpdateCategory(dynamic parameters)
        {
            var category = this.Bind<ProductCategory>();
            await _categoryService.Update(category);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> DeleteCategory(dynamic parameters)
        {
            Guid categoryId = parameters.id;
            await _categoryService.Remove(categoryId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
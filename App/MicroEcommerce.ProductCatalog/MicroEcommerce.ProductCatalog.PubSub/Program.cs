﻿using System;
using MicroEcommerce.Logging;
using MicroEcommerce.ProductCatalog.BusinessLayer;
using MicroEcommerce.ProductCatalog.Infrastructure;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroEcommerce.ProductCatalog.PubSub
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "ProductCatalog PubSub";
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            
            var configuration = builder.Build();
            
            var container = new Container();
            var logger = LoggerConfig.Create();
            var package = new Package();
            
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            container.Register(() => configuration);
            container.Register(() => logger);

            using (AsyncScopedLifestyle.BeginScope(container))
            {
                package.RegisterServices(container);

                var productCatalogPubSub = new ProductCatalogSubscriber(container.GetInstance<IProductService>(), 
                    container.GetInstance<IReceiveExchange>(),
                    container.GetInstance<ILogger>());

                productCatalogPubSub.RegisterExchanges();

                Console.WriteLine("ProductCatalog PubSub is Up");

                Console.ReadLine();
            }
        }
    }
}
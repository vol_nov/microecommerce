﻿using System;
using System.Text;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.BusinessLayer;
using MicroEcommerce.PubSub;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using Serilog;

namespace MicroEcommerce.ProductCatalog.PubSub
{
    public class ProductCatalogSubscriber : ISubscriber
    {
        private readonly IReceiveExchange _receiveExchange;
        private readonly IProductService _productService;
        private readonly ILogger _logger;

        public ProductCatalogSubscriber(IProductService productService, 
            IReceiveExchange receiveExchange,
            ILogger logger)
        {
            _receiveExchange = receiveExchange;
            _productService = productService;
            _logger = logger;
        }

        public async Task OnProductPriceUpdated(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProductPrice = Encoding.UTF8.GetString(body);
            dynamic productPrice = JsonConvert.DeserializeObject(jsonProductPrice);
            
            var product = await _productService.GetItemById((Guid) productPrice.ProductId);
            product.Price = (decimal)productPrice.Price;
            await _productService.Update(product);
            
            _logger.Information($"Product price update: {jsonProductPrice}");
        }

        public void RegisterExchanges()
        {
            _receiveExchange.Receive("productPriceUpdated", (sender, eventArgs) =>
            {
                OnProductPriceUpdated(sender, eventArgs).Wait();
            });
        }
    }
}
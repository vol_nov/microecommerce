﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MicroEcommerce.ProductCatalog.Models
{
    [Serializable]
    public class ProductAttachment
    {
        [BsonId]
        public Guid AttachmentId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public Guid ProductId { get; set; }
    }
}
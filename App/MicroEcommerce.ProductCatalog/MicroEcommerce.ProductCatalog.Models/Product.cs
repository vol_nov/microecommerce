﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MicroEcommerce.ProductCatalog.Models
{
    [Serializable]
    public class Product
    {
        [BsonId]
        public Guid ProductId { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Manufacturer { get; set; }
        public dynamic AdditionalInfo { get; set; }
        public double Count { get; set; }
        public string Unit { get; set; }
        public ICollection<int> Likes { get; set; }
        public ICollection<ProductCategory> Categories { get; set; }
        [BsonIgnore]
        public ICollection<ProductComment> Comments { get; set; }
        [BsonIgnore]
        public ICollection<ProductAttachment> Attachments { get; set; }
    }
}
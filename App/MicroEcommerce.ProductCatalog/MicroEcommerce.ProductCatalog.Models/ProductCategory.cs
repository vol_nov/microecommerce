﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MicroEcommerce.ProductCatalog.Models
{
    [Serializable]
    public class ProductCategory
    {
        [BsonId]
        public Guid CategoryId { get; set; }
        public string Name { get; set; }
    }
}
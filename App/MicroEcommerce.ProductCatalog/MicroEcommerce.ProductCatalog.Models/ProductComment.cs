﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MicroEcommerce.ProductCatalog.Models
{
    [Serializable]
    public class ProductComment
    {
        [BsonId]
        public Guid CommentId { get; set; }
        public int CreatorId { get; set; }
        public string Content { get; set; }
        public DateTime CreationDate { get; set; }
        public Guid ProductId { get; set; }
    }
}
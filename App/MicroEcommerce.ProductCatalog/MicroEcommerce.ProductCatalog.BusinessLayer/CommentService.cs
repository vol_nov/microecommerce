﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.Models;
using MicroEcommerce.ProductCatalog.Persistence;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace MicroEcommerce.ProductCatalog.BusinessLayer
{
    public interface ICommentService : ICrudService<ProductComment>
    {
        Task<ICollection<ProductComment>> GetProductComments(Guid productId);
    }
    
    public class CommentService: ICommentService
    {
        private readonly IProductCommentRepository _productCommentRepository;
        private readonly ISendExchange _sendExchange;
        
        public CommentService(IProductCommentRepository productCommentRepository,
            ISendExchange sendExchange)
        {
            _productCommentRepository = productCommentRepository;
            _sendExchange = sendExchange;
        }
        
        public async Task Add(ProductComment item)
        {
            await _productCommentRepository.Add(item);
            _sendExchange.Send("productCommented", JsonConvert.SerializeObject(item));
        }

        public async Task Remove(ProductComment item)
        {
            await _productCommentRepository.Delete(item);
        }

        public async Task Remove(Guid id)
        {
            await _productCommentRepository.Delete(id);
        }

        public async Task Update(ProductComment item)
        {
            await _productCommentRepository.Update(item);
        }

        public async Task<ICollection<ProductComment>> GetItems()
        {
            return await _productCommentRepository.GetAll();
        }

        public async Task<ProductComment> GetItemById(Guid id)
        {
            return await _productCommentRepository.GetItemById(id);
        }

        public async Task<ICollection<ProductComment>> GetProductComments(Guid productId)
        {
            return await _productCommentRepository.GetProductComments(productId);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.Models;
using MicroEcommerce.ProductCatalog.Persistence;
using Microsoft.Extensions.Configuration;

namespace MicroEcommerce.ProductCatalog.BusinessLayer
{
    public interface IAttachmentService : ICrudService<ProductAttachment>
    {
        Task<IEnumerable<ProductAttachment>> GetProductAttachments(Guid productId);
    }
    
    public class AttachmentService: IAttachmentService
    {
        private readonly IProductAttachmentRepository _productAttachmentRepository;
    
        public AttachmentService(IProductAttachmentRepository productAttachmentRepository)
        {
            _productAttachmentRepository = productAttachmentRepository;
        }
        
        public async Task Add(ProductAttachment item)
        {
            await _productAttachmentRepository.Add(item);
        }

        public async Task Remove(ProductAttachment item)
        {
            await _productAttachmentRepository.Delete(item);
        }

        public async Task Remove(Guid id)
        {
            await _productAttachmentRepository.Delete(id);
        }

        public async Task Update(ProductAttachment item)
        {
            await _productAttachmentRepository.Update(item);
        }

        public async Task<ICollection<ProductAttachment>> GetItems()
        {
            return await _productAttachmentRepository.GetAll();
        }

        public async Task<ProductAttachment> GetItemById(Guid id)
        {
            return await _productAttachmentRepository.GetItemById(id);
        }

        public async Task<IEnumerable<ProductAttachment>> GetProductAttachments(Guid productId)
        {
            return await _productAttachmentRepository.GetProductAttachments(productId);
        }
    }
}
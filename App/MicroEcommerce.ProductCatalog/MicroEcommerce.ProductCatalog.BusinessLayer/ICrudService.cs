﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MicroEcommerce.ProductCatalog.BusinessLayer
{
    public interface ICrudService<T> where T: class
    {
        Task Add(T item);
        Task Remove(T item);
        Task Remove(Guid id);
        Task Update(T item);
        Task<ICollection<T>> GetItems();
        Task<T> GetItemById(Guid id);
    }
}
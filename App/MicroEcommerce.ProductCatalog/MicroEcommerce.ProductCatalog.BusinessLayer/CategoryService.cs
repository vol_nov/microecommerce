﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.Models;
using MicroEcommerce.ProductCatalog.Persistence;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace MicroEcommerce.ProductCatalog.BusinessLayer
{
    public interface ICategoryService : ICrudService<ProductCategory>
    {
        
    }
    
    public class CategoryService: ICategoryService
    {
        private readonly IProductCategoryRepository _productCategoryRepository;
        private readonly ISendExchange _sendExchange;
        
        public CategoryService(IProductCategoryRepository productCategoryRepository,
            ISendExchange sendExchange)
        {
            _productCategoryRepository = productCategoryRepository;
            _sendExchange = sendExchange;
        }
        
        public async Task Add(ProductCategory item)
        {
            await _productCategoryRepository.Add(item);
            _sendExchange.Send("productCategoryAdded", JsonConvert.SerializeObject(item));
        }

        public async Task Remove(ProductCategory item)
        {
            await _productCategoryRepository.Delete(item);
            _sendExchange.Send("productCategoryDeleted", JsonConvert.SerializeObject(item));
        }

        public async Task Remove(Guid id)
        {
            await _productCategoryRepository.Delete(id);
        }

        public async Task Update(ProductCategory item)
        {
            await _productCategoryRepository.Update(item);
        }

        public async Task<ICollection<ProductCategory>> GetItems()
        {
            return await _productCategoryRepository.GetAll();
        }

        public async Task<ProductCategory> GetItemById(Guid id)
        {
            return await _productCategoryRepository.GetItemById(id);
        }
    }
}
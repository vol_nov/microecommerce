﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.ProductCatalog.Models;
using MicroEcommerce.ProductCatalog.Persistence;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using Newtonsoft.Json;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace MicroEcommerce.ProductCatalog.BusinessLayer
{
    public interface IProductService: ICrudService<Product>
    {
        Task<ICollection<Product>> GetProductsByPrice(decimal min, decimal max);
        Task<ICollection<Product>> GetProductsByCategoryId(Guid categoryId);
        Task<ICollection<Product>> SearchProducts(string filter);
        Task<bool> Like(Guid productId, int userId);
    }
    
    public class ProductService: IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductCommentRepository _commentRepository;
        private readonly IProductAttachmentRepository _attachmentRepository;
        private readonly ISendExchange _sendExchange;
        
        public ProductService(IProductRepository productRepository, 
            IProductCommentRepository commentRepository,
            IProductAttachmentRepository attachmentRepository,
            ISendExchange sendExchange)
        {
            _productRepository = productRepository;
            _commentRepository = commentRepository;
            _attachmentRepository = attachmentRepository;
            _sendExchange = sendExchange;
        }
        
        public async Task Add(Product product)
        {
            await _productRepository.Add(product);
            _sendExchange.Send("productAdded", JsonConvert.SerializeObject(product));
        }

        public async Task Remove(Product product)
        {
            await _productRepository.Delete(product);
            _sendExchange.Send("productDeleted", JsonConvert.SerializeObject(product));
        }

        public async Task Remove(Guid id)
        {
            await _productRepository.Delete(id);
            _sendExchange.Send("productDeleted", JsonConvert.SerializeObject(new { ProductId = id }));
        }

        public async Task Update(Product product)
        {
            await _productRepository.Update(product);
        }

        public async Task<ICollection<Product>> GetItems()
        {
            return await await _productRepository.GetAll().ContinueWith(GetProductsWithCommentsAndAttachments);
        }

        private async Task<ICollection<Product>> GetProductsWithCommentsAndAttachments(Task<ICollection<Product>> productsTask)
        {
            var products = await productsTask;
            ParallelLoopResult result = Parallel.ForEach(products, product =>
            {
                var commentsTask = _commentRepository.GetProductComments(product.ProductId);
                var attachmentsTask = _attachmentRepository.GetProductAttachments(product.ProductId);

                Task.WaitAll(commentsTask, attachmentsTask);

                product.Comments = commentsTask.Result;
                product.Attachments = attachmentsTask.Result;
            });
            return products;
        }

        public async Task<Product> GetItemById(Guid id)
        {
            return await _productRepository.GetItemById(id);
        }

        public async Task<ICollection<Product>> GetProductsByPrice(decimal min, decimal max)
        {
            return await _productRepository.GetProductsByPrice(min, max);
        }

        public async Task<ICollection<Product>> GetProductsByCategoryId(Guid category)
        {
            return await _productRepository.GetProductsByCategoryId(category);
        }

        public async Task<ICollection<Product>> SearchProducts(string filter)
        {
            return await _productRepository.SearchProducts(filter);
        }

        public async Task<bool> Like(Guid productId, int userId)
        {
            var result = await _productRepository.Like(productId, userId);
            var product = await _productRepository.GetItemById(productId);
            
            _sendExchange.Send("productLiked", JsonConvert.SerializeObject(new { ProductId = productId, UserId = userId, TotalPrice = product.Price, LikeResult = result }));
            return result;
        }
    }
}
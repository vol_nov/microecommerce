﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.BusinessLayer;
using MicroEcommerce.PriceCalculation.Models;
using MicroEcommerce.PubSub;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using Serilog;

namespace MicroEcommerce.PriceCalculation.PubSub
{
    public class PriceCalculationSubscriber : ISubscriber
    {
        private readonly IReceiveExchange _receiveExchange;
        private readonly IPriceService _priceService;
        private readonly ICurrencyService _currencyService;
        private readonly ILogger _logger;

        public PriceCalculationSubscriber(IPriceService priceService, 
            ICurrencyService currencyService, 
            IReceiveExchange receiveExchange, 
            ILogger logger)
        {
            _receiveExchange = receiveExchange;
            _priceService = priceService;
            _currencyService = currencyService;
            _logger = logger;
        }

        public async Task OnAddProduct(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);

            dynamic product = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(body));

            await _priceService.Add(new ProductPrice()
            {
                ProductId = (Guid) product.ProductId,
                BasePrice = (decimal)product.Price,
                CurrencyId = (await _currencyService.GetItems()).FirstOrDefault()?.CurrencyId ?? 0
            });

            _logger.Information($"Product added: {jsonProduct}");
        }

        public async Task OnDeleteProduct(object sender, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body;
            var jsonProduct = Encoding.UTF8.GetString(body);

            dynamic product = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(body));
            await _priceService.Delete((Guid) product.ProductId);

            _logger.Information($"Product deleted: {jsonProduct}");
        }

        public void RegisterExchanges()
        {
            _receiveExchange.Receive("productAdded", (sender, eventArgs) =>
            {
                OnAddProduct(sender, eventArgs).Wait();
            });
            
            _receiveExchange.Receive("productDeleted", (sender, eventArgs) =>
            {
                OnDeleteProduct(sender, eventArgs).Wait();
            });
        }
    }
}
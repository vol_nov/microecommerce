﻿using System;
using System.Text;
using MicroEcommerce.Logging;
using MicroEcommerce.PriceCalculation.BusinessLayer;
using MicroEcommerce.PriceCalculation.Infrastructure;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroEcommerce.PriceCalculation.PubSub
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "PriceCalculation PubSub";
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            
            var configuration = builder.Build();
            
            var container = new Container();
            var package = new Package();
            var logger = LoggerConfig.Create();

            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            container.Register(() => configuration);
            container.Register(() => logger);
            
            using (AsyncScopedLifestyle.BeginScope(container))
            {
                package.RegisterServices(container);

                var productCatalogPubSub = new PriceCalculationSubscriber(container.GetInstance<IPriceService>(),
                    container.GetInstance<ICurrencyService>(), 
                    container.GetInstance<IReceiveExchange>(),
                    container.GetInstance<ILogger>());

                productCatalogPubSub.RegisterExchanges();

                Console.WriteLine("PriceCalculation PubSub is Up");

                Console.ReadLine();
            }
        }
    }
}
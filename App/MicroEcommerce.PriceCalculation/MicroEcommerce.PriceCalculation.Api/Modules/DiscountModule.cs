﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.BusinessLayer;
using MicroEcommerce.PriceCalculation.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.PriceCalculation.Modules
{
    public class DiscountModule: NancyModule
    {
        private readonly IDiscountService _discountService;
        
        public DiscountModule(IDiscountService discountService) : base("/api/calc")
        {
            _discountService = discountService;
            
            Get("/discounts", GetDiscounts);
            Get("/discounts/{id}", GetDiscountById);
            Get("/discounts/product/{id}", GetDiscountByProductId);
            Post("/discounts", AddDiscount);
            Put("/discounts", UpdateDiscount);
            Delete("/discounts/{id}", DeleteDiscount);
        }

        public async Task<object> GetDiscounts(dynamic parameters)
        {
            var discounts = await _discountService.GetItems();
            return Negotiate.WithModel(discounts).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetDiscountById(dynamic parameters)
        {
            int discountId = parameters.id;
            var discount = await _discountService.GetItemById(discountId);
            return Negotiate.WithModel(discount).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetDiscountByProductId(dynamic parameters)
        {
            Guid productId = parameters.id;
            var discount = await _discountService.GetDiscountByProductId(productId);
            return Negotiate.WithModel(discount).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> AddDiscount(dynamic parameters)
        {
            var discount = this.Bind<Discount>();
            await _discountService.Add(discount);
            return Negotiate.WithModel(discount).WithStatusCode(HttpStatusCode.Created);
        }
        
        public async Task<object> UpdateDiscount(dynamic parameters)
        {
            var discount = this.Bind<Discount>();
            await _discountService.Update(discount);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> DeleteDiscount(dynamic parameters)
        {
            int discountId = parameters.id;
            await _discountService.Delete(discountId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
﻿using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.BusinessLayer;
using MicroEcommerce.PriceCalculation.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.PriceCalculation.Modules
{
    public class ForexModule: NancyModule
    {
        private readonly IForexService _forexService;
        
        public ForexModule(IForexService forexService) : base("/api/calc")
        {
            _forexService = forexService;
            
            Get("/forexes", GetForexes);
            Get("/forexes/{id}", GetForexById);
            Post("/forexes", AddForex);
            Put("/forexes", UpdateForex);
            Delete("/forexes/{id}", DeleteForex);
        }
        
        public async Task<object> GetForexes(dynamic parameters)
        {
            var forexes = await _forexService.GetItems();
            return Negotiate.WithModel(forexes).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetForexById(dynamic parameters)
        {
            int forexId = parameters.id;
            var forex = await _forexService.GetItemById(forexId);
            return Negotiate.WithModel(forex).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> AddForex(dynamic parameters)
        {
            var forex = this.Bind<ForexRate>();
            await _forexService.Add(forex);
            return Negotiate.WithModel(forex).WithStatusCode(HttpStatusCode.Created);
        }
        
        public async Task<object> UpdateForex(dynamic parameters)
        {
            var forex = this.Bind<ForexRate>();
            await _forexService.Update(forex);
            return Negotiate.WithModel(forex).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> DeleteForex(dynamic parameters)
        {
            int forexId = parameters.id;
            await _forexService.Delete(forexId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
﻿using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.BusinessLayer;
using MicroEcommerce.PriceCalculation.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.PriceCalculation.Modules
{
    public class CurrencyModule : NancyModule
    {
        private readonly ICurrencyService _currencyService;
        
        public CurrencyModule(ICurrencyService currencyService) : base("/api/calc")
        {
            _currencyService = currencyService;
            
            Get("/currencies", GetCurrencies);
            Get("/currencies/{id}", GetCurrencyById);
            Post("/currencies", AddCurrency);
            Put("/currencies", UpdateCurrency);
            Delete("/currencies/{id}", DeleteCurrency);
        }
        
        public async Task<object> GetCurrencies(dynamic parameters)
        {
            var currencies = await _currencyService.GetItems();
            return Negotiate.WithModel(currencies).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetCurrencyById(dynamic parameters)
        {
            var currency = await _currencyService.GetItemById((int) parameters.id);
            return Negotiate.WithModel(currency).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> AddCurrency(dynamic parameters)
        {
            var currency = this.Bind<Currency>();
            await _currencyService.Add(currency);
            return Negotiate.WithModel(currency).WithStatusCode(HttpStatusCode.Created);
        }
        
        public async Task<object> UpdateCurrency(dynamic parameters)
        {
            var currency = this.Bind<Currency>();
            await _currencyService.Update(currency);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> DeleteCurrency(dynamic parameters)
        {
            int currencyId = parameters.id;
            await _currencyService.Delete(currencyId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
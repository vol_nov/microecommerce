﻿using System;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.BusinessLayer;
using MicroEcommerce.PriceCalculation.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.PriceCalculation.Modules
{
    public class PriceModule: NancyModule
    {
        private readonly IPriceService _priceService;
        
        public PriceModule(IPriceService priceService) : base("/api/calc")
        {
            _priceService = priceService;
            
            Get("/prices", GetPrices);
            Get("/prices/{productId}", GetPriceForProduct);
            Post("/prices", AddPriceForProduct);
            Put("/prices", UpdatePriceForProduct);
            Delete("/prices/{id}", DeletePriceForProduct);
            Delete("/prices/{productId}/currency/{currencyId}", DeletePriceForProductAndCurrency);
        }

        public async Task<object> GetPrices(dynamic parameters)
        {
            var prices = await _priceService.GetItems();
            return Negotiate.WithModel(prices).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetPriceForProduct(dynamic parameters)
        {
            Guid productId = parameters.productId;

            var productPrice = await _priceService.GetItemById(productId);
            
            return Negotiate.WithModel(productPrice).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> AddPriceForProduct(dynamic parameters)
        {
            var price = this.Bind<ProductPrice>();
            await _priceService.Add(price);
            return Negotiate.WithModel(price).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> UpdatePriceForProduct(dynamic parameters)
        {
            var price = this.Bind<ProductPrice>();
            await _priceService.Update(price);
            return Negotiate.WithModel(price).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> DeletePriceForProduct(dynamic parameters)
        {
            Guid productId = parameters.id;
            await _priceService.Delete(productId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> DeletePriceForProductAndCurrency(dynamic parameters)
        {
            Guid productId = parameters.productId;
            int currencyId = parameters.currencyId;
            await _priceService.Delete(productId, currencyId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
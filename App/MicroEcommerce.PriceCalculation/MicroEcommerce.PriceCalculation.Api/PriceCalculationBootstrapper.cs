﻿using System.Collections.Generic;
using MicroEcommerce.PriceCalculation.Infrastructure;
using MicroEcommerce.SimpleInjector.Nancy;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector.Packaging;

namespace MicroEcommerce.PriceCalculation
{
    public class PriceCalculationBootstrapper : SimpleInjectorNancyBootstrapper
    {
        public PriceCalculationBootstrapper(IConfigurationRoot configuration, ILogger logger) : base(configuration, logger)
        {
            Packages = new List<IPackage> { new Package() };
        }
    }
}
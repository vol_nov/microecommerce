﻿using System;

namespace MicroEcommerce.PriceCalculation.Models
{
    [Serializable]
    public class Currency
    {
        public int CurrencyId { get; set; }
        public string Name { get; set; }
    }
}
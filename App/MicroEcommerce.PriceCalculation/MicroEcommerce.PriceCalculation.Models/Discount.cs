﻿using System;

namespace MicroEcommerce.PriceCalculation.Models
{
    [Serializable]
    public class Discount
    {
        public int DiscountId { get; set; }
        public Guid ProductId { get; set; }
        public decimal Value { get; set; }
    }
}
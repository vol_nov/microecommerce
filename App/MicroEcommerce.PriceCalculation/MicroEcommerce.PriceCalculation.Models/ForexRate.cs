﻿using System;

namespace MicroEcommerce.PriceCalculation.Models
{
    [Serializable]
    public class ForexRate
    {
        public int ForexRateId { get; set; }
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }
        public decimal Coefficient { get; set; }
    }
}
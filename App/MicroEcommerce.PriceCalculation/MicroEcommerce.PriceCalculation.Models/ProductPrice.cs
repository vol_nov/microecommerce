﻿using System;

namespace MicroEcommerce.PriceCalculation.Models
{
    [Serializable]
    public class ProductPrice
    {
        public Guid ProductId { get; set; }
        public decimal BasePrice { get; set; }
        public decimal? CalculatedPrice { get; set; }
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }
    }
}
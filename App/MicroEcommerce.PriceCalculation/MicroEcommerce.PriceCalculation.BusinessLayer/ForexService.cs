﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.Models;
using MicroEcommerce.PriceCalculation.Persistence;

namespace MicroEcommerce.PriceCalculation.BusinessLayer
{
    public interface IForexService : ICrudService<ForexRate, int>
    {
        Task<decimal> Convert(int currencySourceId, int currencyDestId, decimal priceSource);
    }
    
    public class ForexService: IForexService
    {
        private readonly IForexRateRepository _forexRateRepository;
        
        public ForexService(IForexRateRepository forexRateRepository)
        {
            _forexRateRepository = forexRateRepository;
        }
        
        public async Task<IEnumerable<ForexRate>> GetItems()
        {
            return await _forexRateRepository.GetEntities();
        }

        public async Task<ForexRate> GetItemById(int id)
        {
            return await _forexRateRepository.GetEntityById(id);
        }

        public async Task Add(ForexRate item)
        {
            await _forexRateRepository.Add(item);
        }

        public async Task Update(ForexRate item)
        {
            await _forexRateRepository.Update(item);
        }

        public async Task Delete(int id)
        {
            await _forexRateRepository.Delete(id);
        }

        public async Task<decimal> Convert(int currencySourceId, int currencyDestId, decimal priceSource)
        {
            if (currencySourceId == currencyDestId)
                return priceSource;
            
            var forexRateSrc = await _forexRateRepository.GetForexRateByCurrencyId(currencySourceId);
            var forexRateDest = await _forexRateRepository.GetForexRateByCurrencyId(currencyDestId);

            return priceSource * forexRateSrc.Coefficient / forexRateDest.Coefficient;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.Models;
using MicroEcommerce.PriceCalculation.Persistence;
using MicroEcommerce.PubSub;
using Newtonsoft.Json;

namespace MicroEcommerce.PriceCalculation.BusinessLayer
{
    public interface IDiscountService : ICrudService<Discount, int>
    {
        Task<IEnumerable<Discount>> GetDiscountByProductId(Guid productId);
    }
    
    public class DiscountService: IDiscountService
    {
        private readonly IDiscountRepository _discountRepository;
        private readonly IPriceService _priceService;
        private readonly ISendExchange _sendExchange;
        
        public DiscountService(IDiscountRepository discountRepository, 
            IPriceService priceService,
            ISendExchange sendExchange)
        {
            _discountRepository = discountRepository;
            _priceService = priceService;
            _sendExchange = sendExchange;
        }
        
        public async Task<IEnumerable<Discount>> GetItems()
        {
            return await _discountRepository.GetEntities();
        }

        public async Task<Discount> GetItemById(int id)
        {
            return await _discountRepository.GetEntityById(id);
        }

        public async Task Add(Discount item)
        {
            await _discountRepository.Add(item);
            var price = await _priceService.GetItemById(item.ProductId);
            _sendExchange.Send("productPriceUpdated", JsonConvert.SerializeObject(new { ProductId = item.ProductId, Price = price }));
            _sendExchange.Send("discountAdded", JsonConvert.SerializeObject(item));
        }

        public async Task Update(Discount item)
        {
            await _discountRepository.Update(item);
        }

        public async Task Delete(int id)
        {
            var discount = await _discountRepository.GetEntityById(id);
            var price = await _priceService.GetItemById(discount.ProductId);
            await _discountRepository.Delete(id);
            _sendExchange.Send("productPriceUpdated", JsonConvert.SerializeObject(new { ProductId = price.ProductId, Price = price }));
            _sendExchange.Send("discountDeleted", JsonConvert.SerializeObject(new { DiscountId = id }));
        }

        public async Task<IEnumerable<Discount>> GetDiscountByProductId(Guid productId)
        {
            return await _discountRepository.GetDiscountByProductId(productId);
        }
    }
}
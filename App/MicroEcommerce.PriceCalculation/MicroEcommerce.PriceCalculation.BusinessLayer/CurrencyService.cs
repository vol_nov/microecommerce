﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.Models;
using MicroEcommerce.PriceCalculation.Persistence;

namespace MicroEcommerce.PriceCalculation.BusinessLayer
{
    public interface ICurrencyService : ICrudService<Currency, int>
    {
        
    }
    
    public class CurrencyService : ICurrencyService
    {
        private readonly ICurrencyRepository _currencyRepository;

        public CurrencyService(ICurrencyRepository currencyRepository)
        {
            _currencyRepository = currencyRepository;
        }
        
        public async Task<IEnumerable<Currency>> GetItems()
        {
            return await _currencyRepository.GetEntities();
        }

        public async Task<Currency> GetItemById(int id)
        {
            return await _currencyRepository.GetEntityById(id);
        }

        public async Task Add(Currency item)
        {
            await _currencyRepository.Add(item);
        }

        public async Task Update(Currency item)
        {
            await _currencyRepository.Update(item);
        }

        public async Task Delete(int id)
        {
            await _currencyRepository.Delete(id);
        }
    }
}
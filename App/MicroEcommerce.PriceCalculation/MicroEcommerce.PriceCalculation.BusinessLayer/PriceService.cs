﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.Models;
using MicroEcommerce.PriceCalculation.Persistence;

namespace MicroEcommerce.PriceCalculation.BusinessLayer
{
    public interface IPriceService : ICrudService<ProductPrice, Guid>
    {
        Task Delete(Guid productId, int currencyId);
    }
    
    public class PriceService: IPriceService
    {
        private readonly IProductPriceRepository _productPriceRepository;
        private readonly IDiscountRepository _discountRepository;
        private readonly IForexService _forexService;
        
        public PriceService(IProductPriceRepository productPriceRepository, IDiscountRepository discountRepository,
            IForexService forexService)
        {
            _productPriceRepository = productPriceRepository;
            _discountRepository = discountRepository;
            _forexService = forexService;
        }
        
        public async Task<IEnumerable<ProductPrice>> GetItems()
        {
            return await _productPriceRepository.GetEntities();
        }

        public async Task<ProductPrice> GetItemById(Guid id)
        {
            var productPrice = await _productPriceRepository.GetEntityById(id);
            var discounts = await _discountRepository.GetDiscountByProductId(id);

            var calculatedDiscounts = discounts.Select(d => d.Value);  
            foreach (var discount in calculatedDiscounts)
            {
                productPrice.CalculatedPrice -= productPrice.CalculatedPrice * discount;
            }
            _productPriceRepository.Update(productPrice);
            return productPrice;
        }

        public async Task Add(ProductPrice item)
        {
            await _productPriceRepository.Add(item);
        }

        public async Task Update(ProductPrice item)
        {
            await _productPriceRepository.Update(item);
        }

        public async Task Delete(Guid productId)
        {
            await _productPriceRepository.Delete(productId);
        }
        
        public async Task Delete(Guid productId, int currencyId)
        {
            await _productPriceRepository.Delete(productId, currencyId);
        }
    }
}
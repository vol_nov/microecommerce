﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MicroEcommerce.PriceCalculation.BusinessLayer
{
    public interface ICrudService<TEntity, TIdentity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetItems();
        Task<TEntity> GetItemById(TIdentity id);
        Task Add(TEntity item);
        Task Update(TEntity item);
        Task Delete(TIdentity id);
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.Models;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.PriceCalculation.Persistence
{
    public interface ICurrencyRepository: IRepository<Currency, int>
    {
        
    }    
    
    public class CurrencyRepository: ICurrencyRepository
    {
        private readonly PriceCalculationContext _context;
        
        public CurrencyRepository(PriceCalculationContext context)
        {
            _context = context;
        }

        public async Task<List<Currency>> GetEntities()
        {
            return await _context.Currencies.ToListAsync();
        }

        public async Task<Currency> GetEntityById(int id)
        {
            return await _context.Currencies.FirstOrDefaultAsync(c => c.CurrencyId == id);
        }

        public async Task<Currency> Add(Currency entity)
        {
            var entityEntry = await _context.Currencies.AddAsync(entity);
            await _context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
            return entity;
        }

        public async Task Update(Currency entity)
        {
            var entityEntry = _context.Currencies.Update(entity);
            await _context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
        }

        public async Task Delete(int id)
        {
            var currencies = await _context.Currencies.Where(c => c.CurrencyId == id).ToListAsync();
            _context.Currencies.RemoveRange(currencies);
            await _context.SaveChangesAsync();
        }
    }
}
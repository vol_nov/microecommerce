﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.PriceCalculation.Persistence
{
    public interface IRepository<TEntity, TIdentity> where TEntity : class
    {
        Task<List<TEntity>> GetEntities();
        Task<TEntity> GetEntityById(TIdentity id);
        Task<TEntity> Add(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(TIdentity id);   
    }
}
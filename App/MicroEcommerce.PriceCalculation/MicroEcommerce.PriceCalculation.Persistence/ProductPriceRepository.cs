﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.ExpressionVisitors.Internal;

namespace MicroEcommerce.PriceCalculation.Persistence
{
    public interface IProductPriceRepository : IRepository<ProductPrice, Guid>
    {
        Task<ProductPrice> GetProductPrice(Guid productId, int currencyId);
        Task Delete(Guid productId, int currencyId);
    }

    public class ProductPriceRepository: IProductPriceRepository
    {
        private readonly PriceCalculationContext _context;
        
        public ProductPriceRepository(PriceCalculationContext context)
        {
            _context = context;
        }

        public async Task<ProductPrice> GetProductPrice(Guid productId, int currencyId)
        {
            return await _context.ProductPrices.Include(d => d.Currency).FirstOrDefaultAsync(pp => pp.ProductId == productId && pp.CurrencyId == currencyId);
        }

        public async Task<List<ProductPrice>> GetEntities()
        {
            return await _context.ProductPrices.Include(d => d.Currency).ToListAsync();
        }

        public async Task<ProductPrice> GetEntityById(Guid id)
        {
            return await _context.ProductPrices.Include(d => d.Currency).FirstOrDefaultAsync(pp => pp.ProductId == id);
        }

        public async Task<ProductPrice> Add(ProductPrice entity)
        {
            var entityEntry = await _context.ProductPrices.AddAsync(entity);
            await _context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
            return entity;
        }

        public async Task Update(ProductPrice entity)
        {
            var entityEntry = _context.ProductPrices.Update(entity);
            await _context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
        }

        public async Task Delete(Guid id)
        {
            var productPrices = await _context.ProductPrices.Where(c => c.ProductId == id).ToListAsync();
            _context.ProductPrices.RemoveRange(productPrices);
            await _context.SaveChangesAsync();
        }
        
        public async Task Delete(Guid productId, int currencyId)
        {
            var productPrices = await _context.ProductPrices.Where(c => c.ProductId == productId && c.CurrencyId == currencyId).ToListAsync();
            _context.ProductPrices.RemoveRange(productPrices);
            await _context.SaveChangesAsync();
        }
    }
}
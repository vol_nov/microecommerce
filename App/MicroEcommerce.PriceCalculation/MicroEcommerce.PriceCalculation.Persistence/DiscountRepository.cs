﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.Models;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.PriceCalculation.Persistence
{
    public interface IDiscountRepository : IRepository<Discount, int>
    {
        Task<List<Discount>> GetDiscountByProductId(Guid productId);
    }
    
    public class DiscountRepository: IDiscountRepository
    {
        private readonly PriceCalculationContext _context;
        
        public DiscountRepository(PriceCalculationContext context)
        {
            _context = context;
        }

        public async Task<List<Discount>> GetDiscountByProductId(Guid productId)
        {
            return await _context.Discounts.Where(d => d.ProductId == productId).ToListAsync();
        }

        public async Task<List<Discount>> GetEntities()
        {
            return await _context.Discounts.ToListAsync();
        }

        public async Task<Discount> GetEntityById(int id)
        {
            return await _context.Discounts.FirstOrDefaultAsync(d => d.DiscountId == id);
        }

        public async Task<Discount> Add(Discount entity)
        {
            var entityEntry = await _context.Discounts.AddAsync(entity);
            await _context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
            return entity;
        }

        public async Task Update(Discount entity)
        {
            var entityEntry = _context.Discounts.Update(entity);
            await _context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
        }

        public async Task Delete(int id)
        {
            var discounts = await _context.Discounts.Where(d => d.DiscountId == id).ToListAsync();
            _context.Discounts.RemoveRange(discounts);
            await _context.SaveChangesAsync();
        }
    }
}
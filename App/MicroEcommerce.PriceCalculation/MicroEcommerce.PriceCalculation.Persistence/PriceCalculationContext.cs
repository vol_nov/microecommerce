﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using MicroEcommerce.PriceCalculation.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace MicroEcommerce.PriceCalculation.Persistence
{
    public class PriceCalculationContext : DbContext, IDesignTimeDbContextFactory<PriceCalculationContext>
    {
        private readonly string _connectionString;
        
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<ForexRate> ForexRates { get; set; }
        public DbSet<ProductPrice> ProductPrices { get; set; }

        public PriceCalculationContext()
        {
            
        }
        
        public PriceCalculationContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        public PriceCalculationContext CreateDbContext(string[] args)
        {
            return new PriceCalculationContext("Server=NOTEBOOK;Database=MicroEcommerce.PriceCalculation;Trusted_Connection=True;");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductPrice>().HasKey(pp => pp.ProductId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Currency>().HasKey(pp => pp.CurrencyId);
            modelBuilder.Entity<ForexRate>().HasKey(pp => pp.ForexRateId);
            modelBuilder.Entity<Discount>().HasKey(pp => pp.DiscountId);
        }
    }
}
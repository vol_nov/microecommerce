﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.PriceCalculation.Models;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.PriceCalculation.Persistence
{
    public interface IForexRateRepository : IRepository<ForexRate, int>
    {
        Task<ForexRate> GetForexRateByCurrencyId(int currencyId);
    }
    
    public class ForexRateRepository: IForexRateRepository
    {
        private readonly PriceCalculationContext _context;
        
        public ForexRateRepository(PriceCalculationContext context)
        {
            _context = context;
        }

        public async Task<ForexRate> GetForexRateByCurrencyId(int currencyId)
        {
            return await _context.ForexRates.Include(d => d.Currency).FirstOrDefaultAsync(rate => rate.CurrencyId == currencyId);
        }

        public async Task<List<ForexRate>> GetEntities()
        {
            return await _context.ForexRates.Include(d => d.Currency).ToListAsync();
        }

        public async Task<ForexRate> GetEntityById(int id)
        {
            return await _context.ForexRates.Include(d => d.Currency).FirstOrDefaultAsync(fr => fr.ForexRateId == id);
        }

        public async Task<ForexRate> Add(ForexRate entity)
        {
            var entityEntry = await _context.ForexRates.AddAsync(entity);
            await _context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
            return entity;
        }

        public async Task Update(ForexRate entity)
        {
            var entityEntry = _context.ForexRates.Update(entity);
            await _context.SaveChangesAsync();
            entityEntry.State = EntityState.Detached;
        }

        public async Task Delete(int id)
        {
            var forexRates = await _context.ForexRates.Where(c => c.ForexRateId == id).ToListAsync();
            _context.ForexRates.RemoveRange(forexRates);
            await _context.SaveChangesAsync();
        }
    }
}
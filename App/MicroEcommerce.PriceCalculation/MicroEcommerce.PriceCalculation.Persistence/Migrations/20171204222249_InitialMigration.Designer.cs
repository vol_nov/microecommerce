﻿// <auto-generated />
using MicroEcommerce.PriceCalculation.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace MicroEcommerce.PriceCalculation.Persistence.Migrations
{
    [DbContext(typeof(PriceCalculationContext))]
    [Migration("20171204222249_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MicroEcommerce.PriceCalculation.Models.Currency", b =>
                {
                    b.Property<int>("CurrencyId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("CurrencyId");

                    b.ToTable("Currencies");
                });

            modelBuilder.Entity("MicroEcommerce.PriceCalculation.Models.Discount", b =>
                {
                    b.Property<int>("DiscountId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CurrencyId");

                    b.Property<Guid>("ProductId");

                    b.Property<decimal>("Value");

                    b.HasKey("DiscountId");

                    b.HasIndex("CurrencyId");

                    b.ToTable("Discounts");
                });

            modelBuilder.Entity("MicroEcommerce.PriceCalculation.Models.ForexRate", b =>
                {
                    b.Property<int>("ForexRateId")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Coefficient");

                    b.Property<int>("CurrencyId");

                    b.HasKey("ForexRateId");

                    b.HasIndex("CurrencyId");

                    b.ToTable("ForexRates");
                });

            modelBuilder.Entity("MicroEcommerce.PriceCalculation.Models.ProductPrice", b =>
                {
                    b.Property<Guid>("ProductId")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("BasePrice");

                    b.Property<decimal>("CalculatedPrice");

                    b.Property<int?>("CurrencyId");

                    b.HasKey("ProductId");

                    b.HasIndex("CurrencyId");

                    b.ToTable("ProductPrices");
                });

            modelBuilder.Entity("MicroEcommerce.PriceCalculation.Models.Discount", b =>
                {
                    b.HasOne("MicroEcommerce.PriceCalculation.Models.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId");
                });

            modelBuilder.Entity("MicroEcommerce.PriceCalculation.Models.ForexRate", b =>
                {
                    b.HasOne("MicroEcommerce.PriceCalculation.Models.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MicroEcommerce.PriceCalculation.Models.ProductPrice", b =>
                {
                    b.HasOne("MicroEcommerce.PriceCalculation.Models.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId");
                });
#pragma warning restore 612, 618
        }
    }
}

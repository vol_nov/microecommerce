﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MicroEcommerce.PriceCalculation.Persistence.Migrations
{
    public partial class FixDiscount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Discounts_Currencies_CurrencyId",
                table: "Discounts");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductPrices_Currencies_CurrencyId",
                table: "ProductPrices");

            migrationBuilder.DropIndex(
                name: "IX_Discounts_CurrencyId",
                table: "Discounts");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Discounts");

            migrationBuilder.AlterColumn<int>(
                name: "CurrencyId",
                table: "ProductPrices",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CalculatedPrice",
                table: "ProductPrices",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AddForeignKey(
                name: "FK_ProductPrices_Currencies_CurrencyId",
                table: "ProductPrices",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "CurrencyId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductPrices_Currencies_CurrencyId",
                table: "ProductPrices");

            migrationBuilder.AlterColumn<int>(
                name: "CurrencyId",
                table: "ProductPrices",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "CalculatedPrice",
                table: "ProductPrices",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "Discounts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Discounts_CurrencyId",
                table: "Discounts",
                column: "CurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Discounts_Currencies_CurrencyId",
                table: "Discounts",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "CurrencyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductPrices_Currencies_CurrencyId",
                table: "ProductPrices",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "CurrencyId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using System.ComponentModel;
using MicroEcommerce.PriceCalculation.BusinessLayer;
using MicroEcommerce.PriceCalculation.Persistence;
using MicroEcommerce.PubSub;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Packaging;
using Container = SimpleInjector.Container;

namespace MicroEcommerce.PriceCalculation.Infrastructure
{
    public class Package : IPackage
    {
        public void RegisterServices(Container container)
        {
            RegisterPersitenceDependencies(container);
            RegisterBusinessLayerDependencies(container);
        }

        public void RegisterPersitenceDependencies(Container container)
        {
            container.Register(() =>
            {
                var connectionString =
                    container.GetInstance<IConfigurationRoot>().GetConnectionString("DefaultConnectionString");

                return new PriceCalculationContext(connectionString);
            }, Lifestyle.Scoped);

            container.Register<ICurrencyRepository, CurrencyRepository>(Lifestyle.Scoped);
            container.Register<IDiscountRepository, DiscountRepository>(Lifestyle.Scoped);
            container.Register<IProductPriceRepository, ProductPriceRepository>(Lifestyle.Scoped);
            container.Register<IForexRateRepository, ForexRateRepository>(Lifestyle.Scoped);
        }

        public void RegisterBusinessLayerDependencies(Container container)
        {
            container.Register<ICurrencyService, CurrencyService>(Lifestyle.Scoped);
            container.Register<IForexService, ForexService>(Lifestyle.Scoped);
            container.Register<IPriceService, PriceService>(Lifestyle.Scoped);
            container.Register<IDiscountService, DiscountService>(Lifestyle.Scoped);

            container.Register<ISendExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new SendExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);

            container.Register<IReceiveExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new ReceiveExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);
        }
    }
}
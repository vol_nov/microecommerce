﻿using System.Threading.Tasks;
using MicroEcommerce.Deliveries.BusinessLayer;
using MicroEcommerce.Deliveries.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Deliveries.Api.Modules
{
    public class DepartmentModule : NancyModule
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentModule(IDepartmentService departmentService) : base("/api/deliveries")
        {
            _departmentService = departmentService;

            Get("/department", GetCities);
            Get("/department/{id}", GetDepartmentById);
            Get("/department/city/{cityId}", GetDepartmentByAddressId);
            Post("/department", AddDepartment);
            Put("/department", UpdateDepartment);
            Delete("/department/{id}", DeleteDepartment);
        }

        public async Task<object> GetCities(dynamic parameters)
        {
            var cities = await _departmentService.GetModels();
            return Negotiate.WithModel(cities).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetDepartmentByAddressId(dynamic parameters)
        {
            var cities = await _departmentService.GetDepartmentByAddressId((int)parameters.cityId);
            return Negotiate.WithModel(cities).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetDepartmentById(dynamic parameters)
        {
            var department = await _departmentService.GetModelById((int) parameters.id);
            return Negotiate.WithModel(department).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddDepartment(dynamic parameters)
        {
            var department = this.Bind<Department>();
            await _departmentService.Add(department);
            return Negotiate.WithModel(department).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateDepartment(dynamic parameters)
        {
            var department = this.Bind<Department>();
            await _departmentService.Update(department);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteDepartment(dynamic parameters)
        {
            int departmentId = parameters.id;
            await _departmentService.Delete(departmentId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
﻿using System.Threading.Tasks;
using MicroEcommerce.Deliveries.BusinessLayer;
using MicroEcommerce.Deliveries.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Deliveries.Api.Modules
{
    public class AddressModule : NancyModule
    {
        private readonly IService<Address> _addressService;

        public AddressModule(IService<Address> addressService) : base("/api/deliveries")
        {
            _addressService = addressService;

            Get("/address", GetAddresses);
            Get("/address/{id}", GetAddressById);
            Post("/address", AddAddress);
            Put("/address", UpdateAddress);
            Delete("/address/{id}", DeleteAddress);
        }

        public async Task<object> GetAddresses(dynamic parameters)
        {
            var addresses = await _addressService.GetModels();
            return Negotiate.WithModel(addresses).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetAddressById(dynamic parameters)
        {
            var address = await _addressService.GetModelById((int) parameters.id);
            return Negotiate.WithModel(address).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddAddress(dynamic parameters)
        {
            var address = this.Bind<Address>();
            await _addressService.Add(address);
            return Negotiate.WithModel(address).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateAddress(dynamic parameters)
        {
            var address = this.Bind<Address>();
            await _addressService.Update(address);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteAddress(dynamic parameters)
        {
            int addressId = parameters.id;
            await _addressService.Delete(addressId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
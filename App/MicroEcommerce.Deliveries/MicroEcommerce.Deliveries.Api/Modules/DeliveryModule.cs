﻿using System.Threading.Tasks;
using MicroEcommerce.Deliveries.BusinessLayer;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Deliveries.Api.Modules
{
    public class DeliveryModule : NancyModule
    {
        private readonly IDeliveryService _deliveryService;

        public DeliveryModule(IDeliveryService deliveryService) : base("/api/deliveries")
        {
            _deliveryService = deliveryService;

            Get("/delivery", GetDeliveries);
            Get("/delivery/{id}", GetDeliveryById);
            Get("/delivery/address/{id}", GetDeliveryByAddressId);
            Get("/delivery/department/{id}", GetDeliveryByDepartmentId);
            Post("/delivery", AddDelivery);
            Put("/delivery", UpdateDelivery);
            Put("/delivery/{id}/delivered/{value}", SetDelivered);
            Delete("/delivery/{id}", DeleteDelivery);
        }

        public async Task<object> GetDeliveries(dynamic parameters)
        {
            var cities = await _deliveryService.GetModels();
            return Negotiate.WithModel(cities).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetDeliveryById(dynamic parameters)
        {
            var delivery = await _deliveryService.GetModelById((int) parameters.id);
            return Negotiate.WithModel(delivery).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetDeliveryByAddressId(dynamic parameters)
        {
            var delivery = await _deliveryService.GetDeliveriesByAddressId((int) parameters.id);
            return Negotiate.WithModel(delivery).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetDeliveryByDepartmentId(dynamic parameters)
        {
            var delivery = await _deliveryService.GetDeliveriesByDepartmentId((int) parameters.id);
            return Negotiate.WithModel(delivery).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> SetDelivered(dynamic parameters)
        {
            var delivery = await _deliveryService.SetDelivered((int) parameters.id, (bool) parameters.value);
            return Negotiate.WithModel(delivery).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddDelivery(dynamic parameters)
        {
            var delivery = this.Bind<Models.Delivery>();
            await _deliveryService.Add(delivery);
            return Negotiate.WithModel(delivery).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateDelivery(dynamic parameters)
        {
            var delivery = this.Bind<Models.Delivery>();
            await _deliveryService.Update(delivery);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteDelivery(dynamic parameters)
        {
            int deliveryId = parameters.id;
            await _deliveryService.Delete(deliveryId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
﻿using System.Threading.Tasks;
using MicroEcommerce.Deliveries.BusinessLayer;
using MicroEcommerce.Deliveries.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Deliveries.Api.Modules
{
    public class CountryModule : NancyModule
    {
        private readonly IService<Country> _countryService;

        public CountryModule(IService<Country> countryService) : base("/api/deliveries")
        {
            _countryService = countryService;

            Get("/country", GetCountries);
            Get("/country/{id}", GetCountryById);
            Post("/country", AddCountry);
            Put("/country", UpdateCountry);
            Delete("/country/{id}", DeleteCountry);
        }

        public async Task<object> GetCountries(dynamic parameters)
        {
            var countries = await _countryService.GetModels();
            return Negotiate.WithModel(countries).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetCountryById(dynamic parameters)
        {
            var country = await _countryService.GetModelById((int) parameters.id);
            return Negotiate.WithModel(country).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddCountry(dynamic parameters)
        {
            var country = this.Bind<Country>();
            await _countryService.Add(country);
            return Negotiate.WithModel(country).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateCountry(dynamic parameters)
        {
            var country = this.Bind<Country>();
            await _countryService.Update(country);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteCountry(dynamic parameters)
        {
            int countryId = parameters.id;
            await _countryService.Delete(countryId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
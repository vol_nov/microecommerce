﻿using System.Threading.Tasks;
using MicroEcommerce.Deliveries.BusinessLayer;
using MicroEcommerce.Deliveries.Models;
using Nancy;
using Nancy.ModelBinding;

namespace MicroEcommerce.Deliveries.Api.Modules
{
    public class CityModule : NancyModule
    {
        private readonly ICityService _cityService;

        public CityModule(ICityService cityService) : base("/api/deliveries")
        {
            _cityService = cityService;

            Get("/city", GetCities);
            Get("/city/{id}", GetCityById);
            Get("/city/country/{countryId}", GetCitiesByCountryId);
            Post("/city", AddCity);
            Put("/city", UpdateCity);
            Delete("/city/{id}", DeleteCity);
        }

        public async Task<object> GetCities(dynamic parameters)
        {
            var cities = await _cityService.GetModels();
            return Negotiate.WithModel(cities).WithStatusCode(HttpStatusCode.OK);
        }
        
        public async Task<object> GetCitiesByCountryId(dynamic parameters)
        {
            var cities = await _cityService.GetCitiesByCountryId((int)parameters.countryId);
            return Negotiate.WithModel(cities).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> GetCityById(dynamic parameters)
        {
            var city = await _cityService.GetModelById((int) parameters.id);
            return Negotiate.WithModel(city).WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> AddCity(dynamic parameters)
        {
            var city = this.Bind<City>();
            await _cityService.Add(city);
            return Negotiate.WithModel(city).WithStatusCode(HttpStatusCode.Created);
        }

        public async Task<object> UpdateCity(dynamic parameters)
        {
            var city = this.Bind<City>();
            await _cityService.Update(city);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        public async Task<object> DeleteCity(dynamic parameters)
        {
            int cityId = parameters.id;
            await _cityService.Delete(cityId);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}
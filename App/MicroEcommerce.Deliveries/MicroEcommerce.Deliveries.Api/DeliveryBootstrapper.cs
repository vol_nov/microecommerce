﻿using MicroEcommerce.Deliveries.Infrastructure;
using MicroEcommerce.SimpleInjector.Nancy;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace MicroEcommerce.Deliveries.Api
{
    public class DeliveryBootstrapper : SimpleInjectorNancyBootstrapper
    {
        public DeliveryBootstrapper(IConfigurationRoot configurationRoot, ILogger logger) : base(configurationRoot, logger)
        {
            Packages = new[] {new Package()};
        }
    }
}
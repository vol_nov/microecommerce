﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using MicroEcommerce.Deliveries.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace MicroEcommerce.Deliveries.Persistence
{
    public class DeliveryContext: DbContext, IDesignTimeDbContextFactory<DeliveryContext>
    {
        private readonly string _connectionString;
        
        public DbSet<Address> Addresses { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<Department> Departments { get; set; }
        
        public DeliveryContext()
        {
            
        }
        
        public DeliveryContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        public DeliveryContext CreateDbContext(string[] args)
        {
            return new DeliveryContext("Server=NOTEBOOK;Database=MicroEcommerce.Deliveries;Trusted_Connection=True;");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Delivery>().HasKey(pp => pp.DeliveryId)
                .HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
            
            modelBuilder.Entity<Address>().HasKey(pp => pp.AddressId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Country>().HasKey(pp => pp.CountryId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);

            //modelBuilder.Entity<Country>().HasMany(country => country.Cities);
            
            modelBuilder.Entity<City>().HasKey(pp => pp.CityId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Department>().HasKey(pp => pp.DepartmentId).HasAnnotation("DatabaseGeneratedAttribute", DatabaseGeneratedOption.Identity);
        }
    }
}
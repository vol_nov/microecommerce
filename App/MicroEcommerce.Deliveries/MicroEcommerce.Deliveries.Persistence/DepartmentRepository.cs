﻿using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Deliveries.Persistence
{
    public class DepartmentRepository : Repository<Department, int, DeliveryContext>
    {
        public DepartmentRepository(DeliveryContext context) : base(context)
        {
        }

        protected override DbSet<Department> Table => Context.Departments;
        protected override IQueryable<Department> Include(DbSet<Department> table)
        {
            return table.Include(d => d.Address);
        }

        public override async Task<Department> GetEntityById(int id)
        {
            return await Table.FirstOrDefaultAsync(d => d.DepartmentId == id);
        }
    }
}
﻿using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Deliveries.Persistence
{
    public class DeliveryRepository : Repository<Delivery, int, DeliveryContext>
    {
        public DeliveryRepository(DeliveryContext context) : base(context)
        {
        }

        protected override DbSet<Delivery> Table => Context.Deliveries;

        protected override IQueryable<Delivery> Include(DbSet<Delivery> table)
        {
            return table.Include(d => d.Address)
                .Include(d => d.Department);
        }

        public override async Task<Delivery> GetEntityById(int id)
        {
            return await Table.FirstOrDefaultAsync(delivery => delivery.DeliveryId == id);
        }
    }
}
﻿using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Deliveries.Persistence
{
    public class CountryRepository : Repository<Country, int, DeliveryContext>
    {
        public CountryRepository(DeliveryContext context) : base(context)
        {
        }

        protected override DbSet<Country> Table => Context.Countries;
        protected override IQueryable<Country> Include(DbSet<Country> table)
        {
            return table.AsQueryable();
        }

        public override async Task<Country> GetEntityById(int id)
        {
            return await Table.FirstOrDefaultAsync(country => country.CountryId == id);
        }
    }
}
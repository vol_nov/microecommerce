﻿using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Deliveries.Persistence
{
    public class CityRepository : Repository<City, int, DeliveryContext>
    {
        public CityRepository(DeliveryContext context) : base(context)
        {
        }

        protected override DbSet<City> Table => Context.Cities;
        protected override IQueryable<City> Include(DbSet<City> table)
        {
            return table.Include(city => city.Country).AsQueryable();
        }

        public override async Task<City> GetEntityById(int id)
        {
            return await Table.FirstOrDefaultAsync(city => city.CityId == id);
        }
    }
}
﻿using System.Linq;
using System.Threading.Tasks;
using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.EntityFrameworkCore;

namespace MicroEcommerce.Deliveries.Persistence
{
    public class AddressRepository : Repository<Address, int, DeliveryContext>
    {
        public AddressRepository(DeliveryContext context) : base(context)
        {
        }

        protected override DbSet<Address> Table => Context.Addresses;

        protected override IQueryable<Address> Include(DbSet<Address> table)
        {
            return table.Include(address => address.City);
        }

        public override async Task<Address> GetEntityById(int id)
        {
            return await Table.FirstOrDefaultAsync(d => d.AddressId == id);
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.Deliveries.BusinessLayer;
using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.Deliveries.Persistence;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Deliveries.BusinessLayer
{
    public interface ICityService : IService<City>
    {
        Task<ICollection<City>> GetCitiesByCountryId(int countryId);
    }
    
    public class CityService : Service<City>, ICityService
    {
        public CityService(IRepository<City, int> repository) : base(repository)
        {
        }

        public async Task<ICollection<City>> GetCitiesByCountryId(int countryId)
        {
            return await Repository.Where(c => c.CountryId == countryId);
        }
    }
}
﻿using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.Deliveries.Persistence;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Deliveries.BusinessLayer
{
    public class CountryService : Service<Country>
    {
        public CountryService(IRepository<Country, int> repository) : base(repository)
        {
        }
    }
}
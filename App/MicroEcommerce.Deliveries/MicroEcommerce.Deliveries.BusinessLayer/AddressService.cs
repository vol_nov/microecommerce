﻿using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Deliveries.BusinessLayer
{
    public class AddressService : Service<Address>
    {
        public AddressService(IRepository<Address, int> addressRepository) : base(addressRepository)
        {
        }
    }
}
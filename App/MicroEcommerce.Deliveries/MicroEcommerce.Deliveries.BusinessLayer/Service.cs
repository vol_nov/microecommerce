﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MicroEcommerce.Deliveries.Persistence;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Deliveries.BusinessLayer
{
    public abstract class Service<T> : IService<T> where T : class, new()
    {
        protected readonly IRepository<T, int> Repository;

        protected Service(IRepository<T, int> repository)
        {
            Repository = repository;
        }

        public async Task<ICollection<T>> GetModels()
        {
            return await Repository.GetEntities();
        }

        public async Task<T> GetModelById(int id)
        {
            return await Repository.GetEntityById(id);
        }

        public async Task<T> Add(T entity)
        {
            return await Repository.Add(entity);
        }

        public async Task Update(T entity)
        {
            await Repository.Update(entity);
        }

        public async Task Delete(int id)
        {
            await Repository.Delete(id);
        }

        public async Task<ICollection<T>> Where(Expression<Func<T, bool>> condition)
        {
            return await Repository.Where(condition);
        }
    }
}
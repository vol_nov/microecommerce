﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.Deliveries.Persistence;
using MicroEcommerce.PubSub;
using MicroEcommerce.RepositoryBase.EFCore;
using Newtonsoft.Json;

namespace MicroEcommerce.Deliveries.BusinessLayer
{
    public interface IDeliveryService : IService<Delivery>
    {
        Task<ICollection<Delivery>> GetDeliveriesByAddressId(int addressId);
        Task<ICollection<Delivery>> GetDeliveriesByDepartmentId(int departmentId);
        Task<bool> SetDelivered(int deliveryId, bool delivered);
    }
    
    public class DeliveryService : Service<Delivery>, IDeliveryService
    {
        private readonly ISendExchange _sendExchange;
        
        public DeliveryService(IRepository<Delivery, int> repository, ISendExchange sendExchange) : base(repository)
        {
            _sendExchange = sendExchange;
        }

        public async Task<ICollection<Delivery>> GetDeliveriesByAddressId(int addressId)
        {
            return await Repository.Where(delivery => delivery.AddressId == addressId);
        }

        public async Task<ICollection<Delivery>> GetDeliveriesByDepartmentId(int departmentId)
        {
            return await Repository.Where(delivery => delivery.DepartmentId == departmentId);
        }

        public async Task<bool> SetDelivered(int deliveryId, bool delivered)
        {
            var delivery = await Repository.GetEntityById(deliveryId);
            delivery.Delivered = delivered;
            await Repository.Update(delivery);
            _sendExchange.Send("productDelivered", JsonConvert.SerializeObject(delivery));
            return delivered;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MicroEcommerce.Deliveries.BusinessLayer
{
    public interface IService<T> where T : class, new()
    {
        Task<ICollection<T>> GetModels();
        Task<T> GetModelById(int id);
        Task<T> Add(T entity);
        Task Update(T entity);
        Task Delete(int id);
        Task<ICollection<T>> Where(Expression<Func<T, bool>> condition);
    }
}
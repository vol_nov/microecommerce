﻿using System.Threading.Tasks;
using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.Deliveries.Persistence;
using MicroEcommerce.RepositoryBase.EFCore;

namespace MicroEcommerce.Deliveries.BusinessLayer
{
    public interface IDepartmentService : IService<Department>
    {
        Task<Department> GetDepartmentByAddressId(int addressId);
    }
    
    public class DepartmentService: Service<Department>, IDepartmentService
    {
        public DepartmentService(IRepository<Department, int> repository) : base(repository)
        {
        }

        public async Task<Department> GetDepartmentByAddressId(int addressId)
        {
            return await Repository.FirstOrDefault(dep => dep.AddressId == addressId);
        }
    }
}
﻿using System;
using MicroEcommerce.Deliveries.BusinessLayer;
using MicroEcommerce.Deliveries.Models;
using MicroEcommerce.Deliveries.Persistence;
using MicroEcommerce.PubSub;
using MicroEcommerce.RepositoryBase.EFCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace MicroEcommerce.Deliveries.Infrastructure
{
    public class Package: IPackage
    {
        public void RegisterServices(Container container)
        {
            RegisterBusinessLayerDependencies(container);
            RegisterPersistenceDependencies(container);
        }

        private void RegisterBusinessLayerDependencies(Container container)
        {
            container.Register<IService<Address>, AddressService>(Lifestyle.Scoped);
            container.Register<IService<Country>, CountryService>(Lifestyle.Scoped);
            container.Register<ICityService, CityService>(Lifestyle.Scoped);
            container.Register<IDepartmentService, DepartmentService>(Lifestyle.Scoped);
            container.Register<IDeliveryService, DeliveryService>(Lifestyle.Scoped);
            
            container.Register<ISendExchange>(() =>
            {
                var config = container.GetInstance<IConfigurationRoot>();
                return new SendExchange(config.GetSection("RabbitMQHost").Value, container.GetInstance<ILogger>());
            }, Lifestyle.Scoped);
        }
        
        private void RegisterPersistenceDependencies(Container container)
        {
            container.Register(() =>
            {
                var connectionString =
                    container.GetInstance<IConfigurationRoot>().GetConnectionString("DefaultConnectionString");
            
                return new DeliveryContext(connectionString);
            }, Lifestyle.Scoped);
            
            container.Register<IRepository<Address, int>, AddressRepository>(Lifestyle.Scoped);
            container.Register<IRepository<City, int>, CityRepository>(Lifestyle.Scoped);
            container.Register<IRepository<Country, int>, CountryRepository>(Lifestyle.Scoped);
            container.Register<IRepository<Department, int>, DepartmentRepository>(Lifestyle.Scoped);
            container.Register<IRepository<Delivery, int>, DeliveryRepository>(Lifestyle.Scoped);
        }
    }
}
﻿using System;

namespace MicroEcommerce.Deliveries.Models
{
    [Serializable]
    public class Delivery
    {
        public int DeliveryId { get; set; }
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
        public int? AddressId { get; set; }
        public Address Address { get; set; }
        public int ShoppingCartId { get; set; }
        public bool Delivered { get; set; }
        public DateTime TimeToBeDelivered { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace MicroEcommerce.Deliveries.Models
{
    [Serializable]
    public class Country
    {
        public short CountryId { get; set; }
        public string Name { get; set; }
    }
}
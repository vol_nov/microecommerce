﻿using System;

namespace MicroEcommerce.Deliveries.Models
{
    [Serializable]
    public class City
    {
        public int CityId { get; set; }
        public string Name { get; set; }
        public short? CountryId { get; set; }
        public Country Country { get; set; }
    }
}
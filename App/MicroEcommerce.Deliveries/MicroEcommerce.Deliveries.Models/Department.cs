﻿using System.Collections.Generic;

namespace MicroEcommerce.Deliveries.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public int AddressId { get; set; }
        public Address Address { get; set; }
    }
}
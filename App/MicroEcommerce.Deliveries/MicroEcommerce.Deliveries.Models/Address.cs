﻿using System;

namespace MicroEcommerce.Deliveries.Models
{
    [Serializable]
    public class Address
    {
        public int AddressId { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
        public string InCityAddress { get; set; }
    }
}
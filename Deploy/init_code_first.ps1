cd ../App/MicroEcommerce.Deliveries/MicroEcommerce.Deliveries.Api

dotnet ef migrations add InitialMigration -p ../MicroEcommerce.Deliveries.Persistence/MicroEcommerce.Deliveries.Persistence.csproj
dotnet ef database update InitialMigration -p ../MicroEcommerce.Deliveries.Persistence/MicroEcommerce.Deliveries.Persistence.csproj

cd ../../MicroEcommerce.PriceCalculation

dotnet ef migrations add FixDiscount -p ../MicroEcommerce.PriceCalculation.Persistence/MicroEcommerce.PriceCalculation.Persistence.csproj
dotnet ef database update FixDiscount -p ../MicroEcommerce.PriceCalculation.Persistence/MicroEcommerce.PriceCalculation.Persistence.csproj

cd ../../MicroEcommerce.Recommendations

dotnet ef migrations add InitialMigration -p ../MicroEcommerce.Recommendations.Persistence/MicroEcommerce.Recommendations.Persistence.csproj
dotnet ef database update InitialMigration -p ../MicroEcommerce.Recommendations.Persistence/MicroEcommerce.Recommendations.Persistence.csproj

cd ../../MicroEcommerce.ShopperTracking

dotnet ef migrations add InitialMigration -p ../MicroEcommerce.ShopperTracking.Persistence/MicroEcommerce.ShopperTracking.Persistence.csproj
dotnet ef database update InitialMigration -p ../MicroEcommerce.ShopperTracking.Persistence/MicroEcommerce.ShopperTracking.Persistence.csproj
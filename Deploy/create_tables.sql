CREATE DATABASE [MicroEcommerce.Deliveries]
GO

CREATE DATABASE [MicroEcommerce.Notifications]
GO

CREATE TABLE [MicroEcommerce.Notifications].[dbo].[Notifications]
(
  [NotificationId] BIGINT PRIMARY KEY IDENTITY (1, 1),
  [Text] NVARCHAR(MAX) NOT NULL,
  [Link] VARCHAR(MAX),
  [ImagePath] VARCHAR(MAX),
  [Type] TINYINT NOT NULL,
  [CreationDate] DATETIME2 NOT NULL,
  [Seen] BIT NOT NULL,
  [UserId] INT NOT NULL
)
GO

CREATE DATABASE [MicroEcommerce.PriceCalculation]
GO

CREATE DATABASE [MicroEcommerce.Recommendations]
GO

CREATE DATABASE [MicroEcommerce.ShopperTracking]
GO

CREATE DATABASE [MicroEcommerce.ShoppingCart]
GO

CREATE TABLE [MicroEcommerce.ShoppingCart].[dbo].[ShoppingCart]
(
  [ShoppingCartId] INT PRIMARY KEY IDENTITY(1,1),
  [UserId] INT NOT NULL,
  [CreationDate] DATETIME NOT NULL,
  [Bought] BIT NOT NULL
)
GO

CREATE TABLE [MicroEcommerce.ShoppingCart].[dbo].[ShoppingCartProduct]
(
  [ShoppingCartId] INT NOT NULL,
  [ProductId] UNIQUEIDENTIFIER NOT NULL,
  [Price] DECIMAL NOT NULL,
  [Count] INT NOT NULL
)
GO

CREATE DATABASE [MicroEcommerce.UserStore]
GO

CREATE TABLE [MicroEcommerce.UserStore].[dbo].[Country]
(
  [CountryId] TINYINT PRIMARY KEY IDENTITY(1, 1),
  [Name] VARCHAR(64) UNIQUE NOT NULL
)
GO

CREATE TABLE [MicroEcommerce.UserStore].[dbo].[City]
(
  [CityId] INT PRIMARY KEY IDENTITY (1, 1),
  [Name] VARCHAR(128) NOT NULL,
  [CountryId] TINYINT NOT NULL,
  CONSTRAINT [FK_Country_City] FOREIGN KEY ([CountryId]) REFERENCES [Country]([CountryId])
)
GO

CREATE TABLE [MicroEcommerce.UserStore].[dbo].[User]
(
  [UserId] INT PRIMARY KEY IDENTITY (1, 1),
  [Email] VARCHAR(254) NOT NULL UNIQUE,
  [Password] NVARCHAR(254) NOT NULL
)
GO

CREATE TABLE [MicroEcommerce.UserStore].[dbo].[UserDetails]
(
  [UserId] INT NOT NULL,
  [FullName] VARCHAR(512),
  [CountryId] TINYINT,
  [CityId] INT,
  [DateOfBirth] DATE,
  CONSTRAINT [FK_User_UserDetails] FOREIGN KEY ([UserId]) REFERENCES [User]([UserId]),
  CONSTRAINT [FK_Country_UserDetails] FOREIGN KEY ([CountryId]) REFERENCES [Country]([CountryId]),
  CONSTRAINT [FK_City_UserDetails] FOREIGN KEY ([CityId]) REFERENCES [City]([CityId])
)
GO
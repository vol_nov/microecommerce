<# Get current directory path #>
$src = (Get-Item -Path "..\App" -Verbose).FullName;

<# Iterate all directories present in the current directory path #>
Get-ChildItem $src -directory | where {$_.PsIsContainer} | Select-Object -Property Name | ForEach-Object {
    $cdProjectDir = [string]::Format("cd /d {0}\{1}\{1}.Api",$src, $_.Name);

    $params=@("/C"; $cdProjectDir; " && dotnet run"; )
    Start-Process -Verb runas "cmd.exe" $params;
} 